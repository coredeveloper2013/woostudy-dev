<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
//Route::get('state-code','Wsapi\WsApiController@stateUpdate');
//Route::get('state-save','Wsapi\WsApiController@stateStore');

Route::get( 'searchCountry/{countryname}', 'Wsapi\WsApiController@loadView' );
Route::get('ws-institute/{country}/{page}/{page2}', 'Wsapi\WsApiController@testMethod' );
//Route::get('ws-institute', 'Wsapi\WsApiController@testMethod' );
//Route::post( 'ws-country', 'Wsapi\WsApiController@storeCountry' );
//Route::post( 'ws-state', 'Wsapi\WsApiController@storeState' );


//Route::post('storeforfullcountry','Wsapi\WsApiController@storeDataForFullName' );

Route::get('updateUsernames','Wsapi\WsApiController@updateUsername');
// Route::get('updateNames','Wsapi\WsApiController@updateName');
