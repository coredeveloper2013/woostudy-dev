<?php

use Faker\Generator as Faker;

$factory->define( \App\IELTSExamLevel::class, function (Faker $faker) {
    return [
        'title' => $faker->randomElement(['IELTS','TOFEL']),
        'status' => $faker->boolean,
        'created_by' => $faker->randomElement( \App\User::pluck( 'id' )->toArray() ),
        'created_at' => $faker->dateTimeThisMonth,
        'approved_by' => $faker->randomElement( \App\User::pluck( 'id' )->toArray() ),
        'approved_at' => $faker->dateTimeThisMonth
    ];
} );
