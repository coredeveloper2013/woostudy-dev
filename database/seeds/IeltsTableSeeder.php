<?php

use App\Ielts;
use Faker\Factory;
use Illuminate\Database\Seeder;

class IeltsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Ielts::truncate();
        $faker = Factory::create();
        foreach (range( 0, 15 ) as $key) {
            Ielts::create( [
                "ielts_exam_level_id" => $faker->randomElement( \App\IELTSExamLevel::pluck( 'id' )->toArray() ),
                "user_id" => rand( 1, 4 ),
                "score" => rand( 20, 1000 ),
                "taken_on" => $faker->date( $format = 'Y-m-d', $max = 'now' ),
                "grade" => ['A', 'B', 'C', 'D', 'D'][rand( 0, 4 )],
            ] );
        }

    }
}
