<?php

use App\Libraries\Helpers;
use Illuminate\Database\Seeder;
use App\User;
use Carbon\Carbon;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      User::truncate();
      foreach (Helpers::USERS as $index => $user) {
        User::create([
            'name' => $user['name'],
            'email' => $user['email'],
            'role_id' => $user['role_id'],
            'password' => bcrypt('secret'),
        ]);
      }
    }
}
