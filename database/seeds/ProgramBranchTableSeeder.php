<?php

use Illuminate\Database\Seeder;

class ProgramBranchTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        ini_set('memory_limit', '1024M');
        $programs = App\Program::get();
        foreach ($programs as $row) {
            $dataArr = array(
                'institute_id' => $row->institute_id,
                'program_id'   => $row->id,
                'address_id'   => $row->address_id
            );
            App\ProgramBranch::create($dataArr);
        }
    }
}
