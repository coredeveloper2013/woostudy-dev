var express = require('express'),
  app = express(),
  XMLHttpRequest = require("xmlhttprequest").XMLHttpRequest,
  xhr = new XMLHttpRequest(),

  local = process.env.NODE_ENV !== 'production';

if (local === false) {
  //Https
  https = require('https'),
    fs = require('fs'),
    privateKey = fs.readFileSync('/var/www/html/ssl/privkey.pem', 'utf8'),
    certificate = fs.readFileSync('/var/www/html/ssl/cert.pem', 'utf8'),
    credentials = {key: privateKey, cert: certificate},
    server = https.createServer(credentials, app),

    io = require('socket.io')(server),
    port = 3300,
    bodyParser = require("body-parser"),
    ipAddress = '0.0.0.0';
} else {
  //Http
  https = require('http'),
    server = https.createServer(app),

    io = require('socket.io')(server),
    port = 3300,
    bodyParser = require("body-parser"),
    ipAddress = '0.0.0.0';
}

var chalk = require('chalk');
server.listen(port, ipAddress, function () {
  console.log('Cron started on port : ' + port);
});


app.get('/', function (req, res) {
  res.sendFile(__dirname + '/package.json');
});


io.on('connection', function (socket) {
  // socket.on('new-msg', function(data) {
  //     io.emit(data.rec.id, data);
  // });

  socket.on('broadcast', function (payload) {
    payload.users.forEach(function (item) {
      io.emit('update-message' + item, payload.messages);
    })
  });

  socket.on('friendRequest', function (payload) {
    io.emit('friendRequest-' + payload.to, payload);
  });
});
