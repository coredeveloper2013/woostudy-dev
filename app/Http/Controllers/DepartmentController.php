<?php

namespace App\Http\Controllers;

use App\Department;
use Illuminate\Http\Request;

class DepartmentController extends Controller
{

    public function index()
    {
        return Department::all();
    }

    public function show($id)
    {
        return Department::findOrFail($id);
    }

    public function store(Request $request)
    {
        $department = new Department();
        $data = $request->all(); //all request data
        $data['created_by'] = auth()->id();
        $department->fill( $data ); //fill all data
        $department->save(); //save to database
        return $department;
    }


    public function update(Request $request, $id)
    {
        $department = Department::findOrFail( $id ); // find model
        $data = $request->all(); //all request data
        $department->fill( $data ); //fill all data
        $department->save(); //save to database
        return $department;
    }
}
