<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Find\FriendController;
use App\Http\Requests\MediaVideoRequest;
use App\Http\Requests\MediaImageRequest;
use App\Post;
use Illuminate\Http\Request;
use DB;

class PostController extends Controller
{

    public function index(FriendController $friendController)
    {
        $friends = $friendController->friendLists();

        array_push( $friends, auth()->id() );

        $posts = Post::whereStatus( true )
            ->with('video')
            ->with('image')
            ->whereIn( 'user_id', $friends )
            ->latest( 'created_at' )
            ->with( ['user', 'comments' => function ($q) {
                return $q->latest( 'created_at' )->with( 'user' );
            }] )->paginate();

        $posts->each( function ($data) {
            if ($data->user->role_id === 3) {
                return $data->user->institute;
            };
        } );

        return response()->json( $posts );
    }

    public function myPost()
    {
        $posts = Post::whereStatus( true )
            ->with('video')
            ->with('image')
            ->where( 'user_id', auth()->id() )
            ->latest( 'created_at' )
            ->with( ['user', 'comments' => function ($q) {
                return $q->latest( 'created_at' )->with( 'user' );
            }] )->paginate();

        $posts->each( function ($data) {
            if ($data->user->role_id === 3) {
                return $data->user->institute;
            };
        } );

        return response()->json( $posts );
    }

    public function countPost()
    {
        $countPost = Post::whereStatus( true )
        ->where( 'user_id', auth()->id() )
        ->count();
        return response()->json( $countPost );
    }

    public function store(Request $request)
    {
        if ( (!isset($request->body) || $request->body=='') && (empty($request->postImages))) {
          return response()->json(['success' => false, 'message' => 'Post text/images cannot be empty!'], 400);
        }

        $post = Post::create( [
            'body' => $request->body,
            'user_id' => auth()->id()
        ]);

        $file = [];
        $images = $request->postImages;
        if (!empty($images)) {
            foreach ($images as $img) {
                //{"title":null,"original_filename":"Screenshot_1.png","size":7717,"mime_type":"image\/png","ext":"png","filename":"03_07_19_44_691000.png"}
                $move = (new MediaController())->moveFile('bulk/'.$img['filename'], 'images/'.$img['filename']);
                if ($move) {
                    $file[] = [
                        'mediable_id' => $post->id,
                        'title' => $img['title'],
                        'filename' => $img['filename'],
                        'ext' => $img['ext'],
                        'mime_type' => $img['mime_type'],
                        'size' => $img['size'],
                        'original_filename' => $img['original_filename'],
                        'mediable_type' => Post::class,
                    ];
                }
            }
        }
        if (!empty($file)) {
            $post->media()->insert( $file );
        }

        $post->user;
        $post->image;

        return response()->json( $post );
    }

    public function update(Request $request, Post $post)
    {
        $post->fill( $request->all() )->save();
        return response()->json( ['message' => 'update successfully'] );
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Post $post
     * @return \Illuminate\Http\Response
     */
    public function destroy(Post $post)
    {
        $post->comments()->detauch();
        $post->delete();
        return response()->json( ['message' => 'Delete successfully'] );
    }

    public function videoPost(MediaVideoRequest $request)
    {
        /* if (!isset($request->body) || $request->body=='') {
          return response()->json(['success' => false, 'message' => 'Post text cannot be empty!'], 400);
        } */

        $post = Post::create( [
            'body' => $request->body,
            'user_id' => auth()->id()
        ] );

        if ($post) {
            $videos = (new MediaController())->videoUpload( $request );
            $videos['mediable_type'] = Post::class;
            $post->media()->create( $videos );
            $post->user;
            $post->video;

            return response()->json( $post );
        }
    }

    public function imagePost(MediaImageRequest $request)
    {
        $images = (new MediaController())->bulkUpload( $request );
        return response()->json(['success' => true, 'message' => $images]);
    }
}
