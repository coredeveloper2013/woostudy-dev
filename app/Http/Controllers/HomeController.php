<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('home');
    }

    public function profile($slug)
    {
        return view('home', compact('slug'));
    }

    public function socialAuthenticationBy()
    {
        return response()->json(session('auth'));
    }

    public function search($key)
    {
        $data = DB::table(DB::raw("(SELECT `id`, `title`, `created_at`, 'courses' AS type FROM `courses` WHERE title LIKE '%".$key."%'
          UNION ALL
          SELECT `id`, `title`, `created_at`, 'educations' AS type FROM `educations` WHERE title LIKE '%".$key."%'
          UNION ALL
          SELECT `id`, `title`, `created_at`, 'departments' AS type FROM `departments` WHERE  title LIKE '%".$key."%' ) AS A"))
          ->orderBy('A.title', 'ASC')->paginate(10);

        return $data;
    }

    public function shortUrl(Request $request, $url)
    {
        $urlData = \App\FriendshipSocialInvite::where('short_url', $url)->first();
        if (empty($urlData)) {
            return view('error-msg')->with('message', 'You redirect an invalid url!');
        }

        return redirect('/public/profile/'.$urlData->user->user_name);
    }
}
