<?php

namespace App\Http\Controllers;

use App\Services\DeleteUsers;
use App\Services\RoleBackUsers;
use Illuminate\Http\Request;

class TestController extends Controller
{
    public $deleteUser;
    public $roleBackUser;

    public function __construct()
    {
        $this->deleteUser = new DeleteUsers();
        $this->roleBackUser = new RoleBackUsers();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $addresses = $this->deleteUser->getAllAddresses();
        dd($addresses);
    }

    public function roleBackUsers()
    {
        $addresses = $this->roleBackUser->getAllAddresses();
        $data = [];
        foreach ($addresses as $row){
            $data[] = $row->id;
        }
        return $data;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
