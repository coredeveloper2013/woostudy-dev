<?php

namespace App\Http\Controllers;

use App\Chatting;
use App\ChattingBlock;
use App\Events\SendMessageEvent;
use App\Notifications\NewMessageNotification;
use App\User;
use Hootlex\Friendships\Models\Friendship;
use Hootlex\Friendships\Status;
use Illuminate\Http\Request;

class ChattingController extends Controller
{
    public function sendMessage(Request $request)
    {
        $message = Chatting::create( [
            'message' => $request->message,
            'from' => auth()->id(),
            'to' => $request->friend_id
        ] );
        if($user=User::find($request->friend_id)){

            //Condition hide For Notification everytime #91
            //if(!$user->online){
                if(!$user->unreadNotifications()->whereType(NewMessageNotification::class)->first())
                $user->notify( (new NewMessageNotification( $user, $message ))->delay( 5 ) );
            //}
        }
        //broadcast(new NewMessage($message))->toOthers();

        broadcast( new SendMessageEvent( $message ) )->toOthers();

        return response()->json( $message );
    }


    public function history($friendId)
    {
        $checkBlock = ChattingBlock::where( function ($q) use ($friendId) {
            $q->where( function ($q) use ($friendId) {
                $q->whereFrom( auth()->id() )->whereTo( $friendId );
            })->orWhere( function ($q) use ($friendId) {
                $q->whereTo( auth()->id() )->whereFrom( $friendId );
            });
        })->where('status', 1)->first();

        $block = '';
        if (!empty($checkBlock)) {
          if ($checkBlock->from==auth()->id()) {
              $block = 'me';
          } else {
              $block = 'friend';
          }
        }

        $histories = Chatting::where( function ($q) use ($friendId) {
            $q->whereFrom( auth()->id() )->whereTo( $friendId );
        })->orWhere( function ($q) use ($friendId) {
            $q->whereTo( auth()->id() )->whereFrom( $friendId );
        })->limit( 20 )->orderBy('id', 'DESC')->get();

        return response()->json(['histories' => $histories, 'block' => $block ]);
    }


    public function update(Request $request, Chatting $chatting)
    {
        $chatting->message = $request->message;
        $chatting->save();
        return response()->json( ["result" => "updated message"] );
    }


    public function destroy(Chatting $chatting)
    {
        try {
            $chatting->delete();
            return response()->json( ["result" => "deleted message"] );
        } catch (\Exception $e) {
        }
        return response()->json( ["result" => "deleted message"] );
    }


    public function blockUnblock(Request $request)
    {
      $message = ChattingBlock::updateOrCreate(
          ['from' => auth()->id(), 'to' => $request->friend_id],
          ['status' => $request->status]
      );
      return response()->json( ["success" => true, 'status' => $request->status] );
    }

    public function chatFriendLists()
    {
        $friends = Chatting::where( 'to', auth()->id() )
            ->orWhere( 'from', auth()->id() )
            ->latest( 'created_at' )
            ->limit( 10 )
            ->distinct( ['from', 'to'] )->get();

        $friend = $friends->pluck( 'from' )->unique()->toArray();
        $friend2 = $friends->pluck( 'to' )->unique()->toArray();

        $users = array_diff( array_unique( array_merge( $friend, $friend2 ) ), [auth()->id()] );

        $unfriend = Friendship::where('recipient_id', auth()->id())->orWhere('sender_id', auth()->id())->whereStatus(Status::DENIED)
            ->pluck( 'sender_id' )->unique()->toArray();
        $unfriend2 = Friendship::where('recipient_id', auth()->id())->orWhere('sender_id', auth()->id())->whereStatus(Status::DENIED)
            ->pluck( 'recipient_id' )->unique()->toArray();
        $unfriends = array_diff( array_unique( array_merge( $unfriend, $unfriend2 ) ), [auth()->id()] );

        $recentUsers = User::whereIn( 'id', $users )->whereNotIn( 'id', $unfriends )->get();
        if ($friends) {
            $recentUsers->map( function ($value, $key) use ($friends) {
                $data = $friends->where( 'from', $value->id )->first();
                $value->setRelation( 'lastChat', $data );
            } );
        }

        return $recentUsers;
    }

}
