<?php

namespace App\Http\Controllers;

use App\EducationLevel;
use Illuminate\Http\Request;

class EducationLevelController extends Controller
{
    public function index()
    {
        return EducationLevel::all();
    }


    public function create(Request $request)
    {

    }

    public function store(Request $request)
    {
        $eduLevel = new EducationLevel();
        $data = $request->all(); //all request data
        $data['created_by'] = auth()->id();
        $eduLevel->fill( $data ); //fill all data
        $eduLevel->save(); //save to database
        return $eduLevel;
    }


    public function show($id)
    {
        //
    }


    public function edit($id)
    {
        //
    }


    public function update(Request $request, $id)
    {
        $eduLevel = EducationLevel::findOrFail( $id ); // find model
        $data = $request->all(); //all request data
        $eduLevel->fill( $data ); //fill all data
        $eduLevel->save(); //save to database
        return $eduLevel;
    }


    public function destroy($id)
    {
        //
    }
}
