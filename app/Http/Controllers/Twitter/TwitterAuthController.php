<?php

namespace App\Http\Controllers\Twitter;


use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
use Thujohn\Twitter\Facades\Twitter;

class TwitterAuthController
{
    public function login()
    {
        $sign_in_twitter = true;
        $force_login = false;

        Twitter::reconfig(['token' => '', 'secret' => '']);

        if(request('hash')){
            session(['hash' => request('hash')]);
        }

        if(request('url')){
            session(['url' => request('url')]);
        }
        $token = Twitter::getRequestToken(route('twitter.callback'));



        // return response()->json(['message' => $token]);
        // exit;

        if (isset($token['oauth_token_secret'])) {
            $url = Twitter::getAuthorizeURL($token, $sign_in_twitter, $force_login);

            Session::put('oauth_state', 'start');
            Session::put('oauth_request_token', $token['oauth_token']);
            Session::put('oauth_request_token_secret', $token['oauth_token_secret']);

            return Redirect::to($url);
        }
        return Redirect::route('twitter.error');
    }

    public function loginCheck()
    {
      if (Session::get('access_token_expire')) {
          $expire = Session::get('access_token_expire');
          if ($expire >= time()) {
              return response()->json( ['status' => true] );
          }
      }
      return response()->json( ['status' => false] );
    }

    public function callback()
    {
        if (Session::has('oauth_request_token')) {
            $request_token = [
                'token' => Session::get('oauth_request_token'),
                'secret' => Session::get('oauth_request_token_secret'),
            ];

            // dd($request_token);
            // exit;

            $data = Twitter::reconfig($request_token);

            $oauth_verifier = false;

            if (Input::has('oauth_verifier')) {
                $oauth_verifier = Input::get('oauth_verifier');
                $token = Twitter::getAccessToken($oauth_verifier);
            }

            if (!isset($token['oauth_token_secret'])) {
                //return Redirect::route('twitter.error')->with('flash_error', 'We could not log you in on Twitter.');
                return Redirect::to('connections/social-media-link?callback=We could not log you in on Twitter.');
            }

            $credentials = Twitter::getCredentials();

            if (is_object($credentials) && !isset($credentials->error)) {
                Session::put('access_token', $token);
                Session::put('access_token_expire', time() + 24*60*60*3);
                Session::put('auth.twitter', true);

                if(session('url')){
                    $url = session('url');
                    session()->forget('url');
                    return redirect($url.'?logged_by=twitter');
                } elseif(session('hash')){
                    $hash = session('hash');
                    session()->forget('hash');
                    return redirect('/connections/find-others-student/'.$hash);
                } else {
                    return Redirect::to('connections/social-media-link?logged_by=twitter')->with('flash_notice', 'Congrats! You\'ve successfully signed in!');
                }
            }

            //return Redirect::route('twitter.error')->with('flash_error', 'Crab! Something went wrong while signing you up!');
            return Redirect::to('connections/social-media-link?callback=We could not log you in on Twitter.');
        }
    }

    public function logout()
    {
        Session::forget('access_token');
        Session::forget('access_token_expire');
        return Redirect::to('/')->with('flash_notice', 'You\'ve successfully logged out!');
    }
}
