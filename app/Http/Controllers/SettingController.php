<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Setting;

class SettingController extends Controller
{
    public function setting(Request $request) {
        $data = Setting::find(1);
        return response()->json( $data, 200 );
    }
}
