<?php

namespace App\Http\Controllers\Wsapi;
use App\State;
use App\Country;
use App\WsApiModel\User;
use App\WsApiModel\Address;
use App\WsApiModel\Program;
use App\ProfileOption;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use App\WsApiModel\Institute;
use App\WsApiModel\InstituteAuthor;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class WsApiController extends Controller
{
    public $faker;
    public function __construct()
    {
        $this->faker = \Faker\Factory::create();
    }

    // public function stateUpdate()
    // {
    //     $file = file_get_contents('countries.json');
    //     $array = json_decode($file);
    //     foreach ($array as $key => $country) {
    //         foreach ($country->states as $con) {
    //             $data = State::where('title', $con->name)->update(['code' => $con->code]);
    //         }
    //     }
    // }

    // public function stateStore()
    // {
    //     $userData = User::select('users.id', 'users.storeapi_id', 'addresses.country_id', 'countries.title', 'countries.code')
    //     ->join('addresses', 'users.id', '=', 'addresses.addressable_id')
    //     ->join('countries', 'addresses.country_id', '=', 'countries.id')
    //     ->where('users.id', '>', 16)
    //     ->where('addresses.country_id', '!=', 182)
    //     ->get();

    //     foreach ($userData as $user) {
    //         $postData = [
    //             'indexCountry' => $user->code,
    //             'output_mode' => 'json',
    //             'item_id' => $user->storeapi_id
    //         ];
    //         $res = json_decode($this->curloperation("http://ws.aaemnow.com/instituteDetails", $postData));

    //         $state = State::where('country_id', $user->country_id)
    //         ->where(function ($q) use ($res) {
    //             $q->Where('title', $res->data->state)
    //             ->orWhere('code', $res->data->state);
    //         })->first();
    //         if (!$state) {
    //         	$state = State::create([
    //         		'country_id' => $user->country_id,
    //         		'title' => $res->data->state,
    //         		'code' => $res->data->state,
    //         	]);
    //         }
    //         $address = Address::where('addressable_id', $user->id)->update(['state_id' => $state->id]);

    //         $error = [];
    //         if (!$address) {
    //             $error[] = $state;
    //         }
    //         //dd($res->data->state);

    //         //Mongo Save...
    //         User::mongoSaveApi(3, $user->id);
    //     }

    //     return response()->json([
    //         'success' => true,
    //         'error' => $error,
    //     ], 200);
    // }

    // public function updateUsername(){
    //     $allUsers = User::where('role_id', 3)->get(['id','user_name'])->map(function($user) {
    //         // $update['user_name'] = preg_replace("/[^A-Za-z0-9?!-]/", '', $user->user_name);
    //         // $data = User::where('id', $user->id)->update($update);
    //         \App\WsApiModel\User::mongoSaveApi(3, $user->id);
    //         // return $data;
    //     });
    //     dd($allUsers);
    // }

    // public function updateName(){
    //     $allUsers = User::where('users.role_id', 3)->where('users.id', '>', 23)->where('addresses.country_id', 182)
    //     ->join('addresses', 'users.id', '=', 'addresses.addressable_id')
    //     ->get(['users.id','users.name'])->map(function($user) {
    //         $update['name'] = str_replace("-", ' ', ucwords($user->name));
    //         $data = User::where('id', $user->id)->update($update);
    //         \App\WsApiModel\User::mongoSaveApi(3, $user->id);
    //         return $data;
    //     });

    //     dd($allUsers);
    // }

    public function loadView( $countryname ){
        $ret =  $this->getDataByContry( $countryname);
        $data = [
            'message' => $ret->total.' Items have been found',
            'data' => $ret->total !=0 ?  $ret->search_results : [],
            'total_item_found' => $ret->total,
            'countryfullname' => $countryname,
            'response' => true,
            'total_page' =>(int) ceil($ret->total/25),
            'load_suggestion' => 'https://woostudy.aaemnow.com/api/ws-institute/'.$countryname.'/1/2'
        ];
        dump($data);
    }

    public function getDataByContry($country)
    {
        $url = "http://ws.aaemnow.com/instituteSearch";
          $postdata  = [
              'country' => $country,
              'output_mode' => 'json'
          ];
          return  json_decode( $this->curloperation($url,$postdata) );
    }

    public function curloperation($url,$dataarray){

        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => json_encode($dataarray, true),
            CURLOPT_HTTPHEADER => array(
                "content-type: application/json",
            ),
        ));
        $response = curl_exec($curl);
        $err = curl_error($curl);
        curl_close($curl);

        if ($err) {
            return null;
        } else {
            return $response;
        }
    }



    public function apiTestMethod($country, $page){
          $url = "http://ws.aaemnow.com/instituteSearch";
          $postdata  = [
              'country' => $country,
              'output_mode' => 'json',
              'page' => $page,
          ];
          return  json_decode( $this->curloperation($url,$postdata) );
    }



    public function testMethod($country,$page, $page2)
    {
        $countries =  Country::all()->map(function($item){
          return $item['code'];
        });

        if( $page2 < $page ) {
            $temp = $page2;
            $page2 = $page;
            $page = $temp;
        }

        $countryName = $country;
        for ($i=$page; $i <= $page2; $i++)
        {
            $returndata =   $this->apiTestMethod($countryName, $i);
            $this->processArray($returndata->search_results);
        }
    }




    public function processArray($search_result){
          /*
            now we are process the array we have found
            all we need is country and the id from that array
            id => AWsaB8FXr24hW7z46ob2 country=> USA
          */
          $ids = collect($search_result)->map(function($item){
              return ['id'=> $item->id, 'country' => $item->country];
          });

        foreach ($ids as $key => $value) {
            $details =  $this->getDetails($value);
            if ( strlen($details->country) != 3  ) {
              $resData = $this->storeDataForFullName($details);
            }
            else{
              $resData = $this->storeData($details);
            }
        }

    }



  public function getDetails($details){

         $url = "http://ws.aaemnow.com/instituteDetails";

         $postdata  = [
            'indexCountry' =>$details['country'],
            'output_mode' => 'json',
            'item_id' => $details['id']
        ];

     $returnDetails = json_decode( $this->curloperation($url,$postdata) );
     $returnDetails->data->storepai_id  = $details['id'];
       return  $returnDetails->data;

  }

  public function getDetailsTwo($details){

         $url = "http://ws.aaemnow.com/instituteDetails";

         $postdata  = [
            'indexCountry' =>$details['country'],
            'output_mode' => 'json',
            'item_id' => $details['id']
        ];

      $returnDetails = json_decode( $this->curloperation($url,$postdata));
       return  $returnDetails->data;

  }








       //  return response()->json(['result'=> [ 'message'=> "success", 'status'=> true, 'data'=> $allcountry ]], 200);

    public function storeData($data)
    {
        $random = Str::random(8);
        $data->user_name = preg_replace("/[^A-Za-z0-9?!-]/", '', strtolower(str_replace(' ','-',$data->institute_name)));
        $valid = Validator::make((array)$data, [
            'email' => 'unique:users',
        ]);
        if ($valid->fails()) {
            $data->email = $data->email.'-'.$random;
        }
        $valid = Validator::make((array)$data, [
            'user_name' => 'unique:users'  //has little bit of confusion here. this field is not being validated
        ]);
        if ($valid->fails()) {
            $data->user_name = $data->user_name.'-'.$random;
        }

        $user = new User();
        $user->name = $data->institute_name;
        $user->user_name =  $data->user_name;
        $user->email = $data->email;
        $user->storeapi_id = $data->storepai_id;
        $user->role_id = 3;
        $user->password = bcrypt('123456');

        $user->save();

        if (!$user) {
            return response()->json(['success'=>false,'mesg'=> 'User creation Failed'], 200);
        }

        $institute = new Institute();
        $institute->title =  $data->institute_name;
        $institute->education_level_id = 11;
        $institute->user_id = $user->id;
        $institute->email = $data->email;
        $institute->phone = $data->phone;
        $institute->created_by = $user->id;
        $institute->save();

        if (!$institute) {
            return response()->json(['success'=>false,'mesg'=> 'Institute creation Failed'], 200);
        }

        $country_id = $this->getCountryId($data->country);
        $state_id = $this->getStateId($data->state, $country_id);
        $address = new Address();
        $address->country_id = $country_id;
        $address->state_id = $state_id;
        $address->city =  $data->city;
        $address->address = $data->street_address;
        $address->lat =$data->geotag->lat;
        $address->lng = $data->geotag->lon;
        $address->addressable_id = $user->id;
        $address->addressable_type = 'App\User';
        $address->save();

        if (!$address) {
            return response()->json(['success'=>false,'mesg'=> 'Address creation Failed'], 200);
        }

        $institute_role = new InstituteAuthor();
        $institute_role->institute_id = $institute->id;
        $institute_role->department_id = 0;
        $institute_role->institute_role_id = 1;
        $institute_role->name = $data->chief_administrator;
        $institute_role->email = $data->email;
        $institute_role->phone = $data->phone;
        $institute_role->mobile = $data->phone;
        $institute_role->created_by = $user->id;
        $institute_role->save();

        if (!$institute_role) {
            return response()->json(['success'=>false,'mesg'=> 'school Role creation Failed'], 200);
        }

        $prgArr = [];
        if (!empty($data->course)) {
            foreach ($data->course as $program) {
                $currency = isset($program->currency)?$program->currency:null;
                $fee = isset($program->fee)?$program->fee:0.00;
                $program =  [
                    'address_id' => $address->id,
                    'education_level_id' => 11,
                    'institute_id' => $institute->id,
                    'title' => $program->degree,
                    'fee' => $fee.' '.$currency,
                    'fee_int' => $fee,
                    'admission_date'=>null,
                    'description' => json_encode($program->program)
                ];
                $prgArr[] = $program;

            }

            if (!empty($prgArr)) {
                Program::insert($prgArr);
            }
        }


        //add profile_options
        //facebook_url
        if(isset($data->facebook_link)) {
            $profileOption = ProfileOption::where('user_id','=',$user->id)
                    ->where('option_key','=','facebook_url')->first();
            if($profileOption) {
                $profileOption->option_value = $data->facebook_link;
            } else {
                $profileOption = new ProfileOption();
                $profileOption->user_id = $user->id;
                $profileOption->option_key = 'facebook_url';
                $profileOption->option_value = $data->facebook_link;
                $profileOption->save();
            }
        }

        //twitter_url
        if(isset($data->twitter_link)) {
            $profileOption = ProfileOption::where('user_id','=',$user->id)
                    ->where('option_key','=','twitter_url')->first();
            if($profileOption) {
                $profileOption->option_value = $data->twitter_link;
            } else {
                $profileOption = new ProfileOption();
                $profileOption->user_id = $user->id;
                $profileOption->option_key = 'twitter_url';
                $profileOption->option_value = $data->twitter_link;
                $profileOption->save();
            }
        }

        //instragram_url
        if(isset($data->instragram_link)) {
            $profileOption = ProfileOption::where('user_id','=',$user->id)
                    ->where('option_key','=','instragram_url')->first();
            if($profileOption) {
                $profileOption->option_value = $data->instragram_link;
            } else {
                $profileOption = new ProfileOption();
                $profileOption->user_id = $user->id;
                $profileOption->option_key = 'instragram_url';
                $profileOption->option_value = $data->instragram_link;
                $profileOption->save();
            }
        }

        //google_url
        if(isset($data->google_link)) {
            $profileOption = ProfileOption::where('user_id','=',$user->id)
                    ->where('option_key','=','google_url')->first();
            if($profileOption) {
                $profileOption->option_value = $data->google_link;
            } else {
                $profileOption = new ProfileOption();
                $profileOption->user_id = $user->id;
                $profileOption->option_key = 'google_url';
                $profileOption->option_value = $data->google_link;
                $profileOption->save();
            }
        }

        //website
        if(isset($data->website)) {
            $profileOption = ProfileOption::where('user_id','=',$user->id)
                    ->where('option_key','=','website')->first();
            if($profileOption) {
                $profileOption->option_value = $data->website;
            } else {
                $profileOption = new ProfileOption();
                $profileOption->user_id = $user->id;
                $profileOption->option_key = 'website';
                $profileOption->option_value = $data->website;
                $profileOption->save();
            }
        }

        //Mongo Save...
        User::mongoSaveApi(3, $user->id);

        return response()->json([
            'success' => true,
        ], 200);
    }

    public function storeDataForFullName($data)
    {
        $random = $this->faker->unique()->numberBetween($min = 1000000, $max = 9000000);
        $data->user_name = preg_replace("/[^A-Za-z0-9?!-]/", '', strtolower(str_replace(' ','-',$data->institute_name)));
        $valid = Validator::make((array)$data, [
            'email' => 'required|email',
        ]);
        if ($valid->fails()) {
            $data->email = 'W'.$random.'@woostudy.com';
        }
        $valid = Validator::make((array)$data, [
            'email' => 'unique:users',
        ]);
        if ($valid->fails()) {
            $data->email = $data->email.'-'.$random;
        }
        $valid = Validator::make((array)$data, [
            'user_name' => 'required|string|unique:users'  //has little bit of confusion here. this field is not being validated
        ]);
        if ($valid->fails()) {
            $data->user_name = $data->user_name.'-'.'W'.$random;
        }


        $user = new User();
        $user->name = $data->institute_name;
        $user->user_name = $data->user_name;
        $user->email = $data->email;
        $user->storeapi_id = $data->storepai_id;
        $user->role_id = 3;
        $user->password = bcrypt('123456');

        $user->save();

        if (!isset($user->id)) {
            return response()->json(['success'=>false,'mesg'=> 'User creation Failed'], 200);
        }

        $institute = new Institute();
        $institute->title =  $data->institute_name;
        $institute->education_level_id = 11;
        $institute->user_id = $user->id;
        $institute->email = $data->email;
        $institute->phone = $data->phone;
        $institute->created_by = $user->id;
        $institute->save();


        $country_id = $this->getCountryId($data->country);
        $state_id = $this->getStateId($data->state, $country_id);
        $address = new Address();
        $address->country_id = $country_id;
        $address->state_id = $state_id;
        $address->city =  $data->city;
        $address->address = $data->street_address;
        $address->lat =$data->geotag->lat;
        $address->lng = $data->geotag->lon;
        $address->addressable_id = $user->id;
        $address->addressable_type = 'App\User';
        $address->save();

        if (!isset($address->id)) {
            return response()->json(['success'=>false,'mesg'=> 'Address creation Failed'], 200);
        }

        $institute_role = new InstituteAuthor();
        $institute_role->institute_id = $institute->id;
        $institute_role->department_id = 0;
        $institute_role->institute_role_id = 1;
        $institute_role->name = $data->institute_name;
        $institute_role->email = $data->email;
        $institute_role->phone = $data->phone;
        $institute_role->mobile = $data->phone;
        $institute_role->created_by = $user->id;
        $institute_role->save();

        if (!isset($institute_role->id)) {
            return response()->json(['success'=>false,'mesg'=> 'school Role creation Failed'], 200);
        }


        $programs = explode("\r\n\r\n", $data->programs);
        $prgArr = [];
        for($i = 0; $i<= ceil( count($programs)/2); $i++)
        {
            $program = [];
            if ( ($i+1) < count($programs) ) {
                $program =  [
                    'address_id' => $address->id,
                    'education_level_id' => 11,
                    'institute_id' => $institute->id,
                    'title' => str_replace("\r\n", ' ', $programs[$i]),
                    'fee' => null,
                    'admission_date'=> null,
                    'description' => str_replace('Fields of study:', '', $programs[$i+1])
                ];
                $prgArr[] = $program;
            }
            $i++;
        }

        if (!empty($prgArr)) {
            Program::insert($prgArr);
        }

        //add profile_options
        //facebook_url
        if(isset($data->facebook_link)) {
            $profileOption = ProfileOption::where('user_id','=',$user->id)
                    ->where('option_key','=','facebook_url')->first();
            if($profileOption) {
                $profileOption->option_value = $data->facebook_link;
            } else {
                $profileOption = new ProfileOption();
                $profileOption->user_id = $user->id;
                $profileOption->option_key = 'facebook_url';
                $profileOption->option_value = $data->facebook_link;
                $profileOption->save();
            }
        }

        //twitter_url
        if(isset($data->twitter_link)) {
            $profileOption = ProfileOption::where('user_id','=',$user->id)
                    ->where('option_key','=','twitter_url')->first();
            if($profileOption) {
                $profileOption->option_value = $data->twitter_link;
            } else {
                $profileOption = new ProfileOption();
                $profileOption->user_id = $user->id;
                $profileOption->option_key = 'twitter_url';
                $profileOption->option_value = $data->twitter_link;
                $profileOption->save();
            }
        }

        //instragram_url
        if(isset($data->instragram_link)) {
            $profileOption = ProfileOption::where('user_id','=',$user->id)
                    ->where('option_key','=','instragram_url')->first();
            if($profileOption) {
                $profileOption->option_value = $data->instragram_link;
            } else {
                $profileOption = new ProfileOption();
                $profileOption->user_id = $user->id;
                $profileOption->option_key = 'instragram_url';
                $profileOption->option_value = $data->instragram_link;
                $profileOption->save();
            }
        }

        //google_url
        if(isset($data->google_link)) {
            $profileOption = ProfileOption::where('user_id','=',$user->id)
                    ->where('option_key','=','google_url')->first();
            if($profileOption) {
                $profileOption->option_value = $data->google_link;
            } else {
                $profileOption = new ProfileOption();
                $profileOption->user_id = $user->id;
                $profileOption->option_key = 'google_url';
                $profileOption->option_value = $data->google_link;
                $profileOption->save();
            }
        }

        //website
        if(isset($data->website)) {
            $profileOption = ProfileOption::where('user_id','=',$user->id)
                    ->where('option_key','=','website')->first();
            if($profileOption) {
                $profileOption->option_value = $data->website;
            } else {
                $profileOption = new ProfileOption();
                $profileOption->user_id = $user->id;
                $profileOption->option_key = 'website';
                $profileOption->option_value = $data->website;
                $profileOption->save();
            }
        }

        //Mongo Save...
        User::mongoSaveApi(3, $user->id);

        return response()->json(['result'=> ['message' => 'Successful', 'response'=>true, 'data' => [
                'instatitute' => $institute,
                'address'=> $address,
                'programs'=> $prgArr
            ]
        ]
    ], 200);

         // return response()->json(['resposne_data'=>$r->all()], 200);
    }

    public function getCountryId($code)
    {
        //checking if the country name has full form
        $countryIndex ='';
        if (!empty($code))
            $countryIndex =   (strlen( (string) $code) == 3 ) ? 'code': 'title';
        else return null;

        $country = Country::where($countryIndex, $code)->first();
        if (!$country) {
            // return null;
            $country = Country::create([
                'title' => $code,
                'code' => $code,
                'region_id' => '',
                'slug' => strtolower(str_replace(' ', '-', $code)),
                'flag' => '',
            ]);
        }
        return $country->id;
    }

    public function getStateId($state, $country_id = null)
    {
        if (empty($state)) return null;

        $stateDb = State::where('title', $state)->first();
        if (!$stateDb) {
            $stateDb = State::create([
                'title' => $state,
                'country_id' => $country_id,
              ]);
            // return null;
        }
        return $stateDb->id;

        // $state =  State::get()->search(function($item, $key) use($statename){
        //     return $item['title'] == $statename;
        // });
    }

    //Store Country Data...
    public function storeCountry(Request $request)
    {
        if (!empty($request->data)) {
            $saveArray = [];
            foreach ($request->data as $val) {
                $saveArray[] = [
                  'title' => $val['name'],
                  'code' => $val['code'],
                  'region_id' => $val['region_id'],
                  'slug' => strtolower(str_replace(' ', '-', $val['name'])),
                  'flag' => '',
                ];
            }
            Country::insert($saveArray);
            return response()->json([
                'success' => true,
                'message' => count($saveArray).' Country inserted successfully!'
            ], 200);
        } else {
            return response()->json([
                'success' => false,
                'message' => 'Country data not found!'
            ], 400);
        }
    }

    //Store State Data...
    public function storeState(Request $request)
    {
        if (!empty($request->data)) {
            $saveArray = [];
            foreach ($request->data as $val) {
                $saveArray[] = [
                  'title' => $val['name'],
                  'country_id' => $val['country_id'],
                ];
            }
            State::insert($saveArray);
            return response()->json([
                'success' => true,
                'message' => count($saveArray).' State inserted successfully!'
            ], 200);
        } else {
            return response()->json([
                'success' => false,
                'message' => 'State data not found!'
            ], 400);
        }
    }
}
