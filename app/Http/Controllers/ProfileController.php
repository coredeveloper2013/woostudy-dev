<?php

namespace App\Http\Controllers;

use App\EducationLevel;
use App\ProfileOption;
use App\Program;
use App\User;
use App\Video;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Hootlex\Friendships\Models\Friendship;

class ProfileController extends Controller
{
    public function index($slug)
    {
        $user = User::where('user_name', $slug)->firstOrFail();
        return view('frontend.profiles.index', compact('user'));
    }

    public function get_auth_user_data()
    {

        if (Auth::check() == false) {
            return [];
        }

        $user = User::with('role', 'address')->where('id', auth()->id())->first();

        if (!$user->isSchool()) {
            $user->educations;
            if ($user->educations->count()) {
                foreach ($user->educations as $institute) {
                    $institute->institute;
                }
            }

        }
        if ($user->isSchool()) {
            $user->institute;
            optional($user->institute)->programs;
            optional($user->institute)->educationLevel;
            $user->instituteAuthor;
            optional($user->instituteAuthor)->department;
            optional($user->instituteAuthor)->instituteRole;
        }

        if ($user->isTutor()) {
            $user->experiences;
            if ($user->experiences->count()) {
                foreach ($user->experiences as $experience) {
                    $experience->institute;
                }
            }
        }

        return $user;
    }

    function get_profile_options($collection, $fields)
    {
        $final_values = [];
        $fields = ['palash', 'sumon'];
        foreach ($fields as $value) {
            $final_values[$value] = '';
        }

        foreach ($users as $user) {
            if (in_array($user->user_name, $fields)) {
                $final_values[$user->user_name] = $user->name;
            }
        }
        return $final_values;
    }

    public function generate_empty_array_value($fields)
    {

        $final_values = [];
        foreach ($fields as $key) {
            $final_values[$key] = '';
        }
        return $final_values;
    }

    public function get_profile_personal_info($profile_id)
    {
        // name, user, gender, date_of_birth
        $user = User::find($profile_id);
        $final_values = [];
        $fields = ['gender', 'date_of_birth'];
        $final_values = $this->get_values_from_options_table_by_fields($fields, $profile_id);
        $final_values['user_name'] = $user->user_name;
        $final_values['name'] = $user->name;
        return $final_values;

    }

    public function post_profile_personal_info()
    {
        $user = auth()->user();
        $date_of_birth = request('date_of_birth');
        $gender = request('gender');
        $name = request('name');
        $user_name = request('user_name');
        $message = [];
        $might_different_user = User::where('user_name', $user_name)->first();

        if ($might_different_user) {
            if ($might_different_user->id != $user->id) {
                $message['error'] = 'user_name all ready taken exists';
                return $message;
            }
        }

        $user->name = $name;
        $user->user_name = $user_name;
        $user->save();
        $this->set_or_update_profile_options('date_of_birth', $date_of_birth);
        $this->set_or_update_profile_options('gender', $gender);
        $message['success'] = 'Profile updated successfully';
        return $message;
    }

    public function set_or_update_profile_options($key, $value)
    {

        $user_id = auth()->id();
        $option = ProfileOption::where('option_key', $key)->where('user_id', $user_id)->first();
        if ($option) {
            $option->option_value = $value;
            $option->save();
        } else {
            ProfileOption::create([
                'user_id' => $user_id,
                'option_key' => $key,
                'option_value' => $value,
            ]);
        }
    }

    public function set_or_update_profile_options_by_fields($fields)
    {
        $option_fields = [];
        foreach ($fields as $value) {
            $option_fields[$value] = request($value);
        }
        foreach ($option_fields as $key => $value) {
            $this->set_or_update_profile_options($key, $value);
        }
        return true;
    }


    public function get_values_from_options_table_by_fields($fields, $id)
    {
        $final_values = [];
        $user = User::find($id);
        $options = ProfileOption::where('user_id', $user->id)->get();
        $final_values = $this->generate_empty_array_value($fields);
        foreach ($options as $option) {
            if (in_array($option->option_key, $fields)) {
                $final_values[$option->option_key] = $option->option_value;
            }
        }
        return $final_values;
    }

    public function get_contact_details($id)
    {
        $fields = ['country', 'city', 'address', 'lat', 'lng'];
        return $this->get_values_from_options_table_by_fields($fields, $id);
    }


    public function get_social_media_details($id)
    {
        $fields = ['facebook_url', 'twitter_url', 'instragram_url', 'google_url'];
        return $this->get_values_from_options_table_by_fields($fields, $id);
    }

    public function post_contact_details()
    {
        $fields = ['country', 'city', 'address', 'lat', 'lng'];
        $this->set_or_update_profile_options_by_fields($fields);
        return response()->json([
            'success' => 'update information successfully'
        ]);

    }

    public function post_social_media_details()
    {
        $fields = ['facebook_url', 'twitter_url', 'instragram_url', 'google_url'];
        $this->set_or_update_profile_options_by_fields($fields);
        return response()->json([
            'success' => 'update information successfully'
        ]);
    }

    public function publicProfile(Request $request, $profileId)
    {

        if (Auth::check()) {
            $user = User::select('users.*', 'friendships.id AS fId')->whereUserName($profileId);
            $user = $user->leftJoin('friendships', function ($join) {
                $join->on('users.id', 'friendships.sender_id');
                $join->orOn('users.id', 'friendships.recipient_id');
                $join->where(function ($q) {
                    $q->where(function ($q) {
                        $q->where('friendships.sender_id', auth()->user()->id)->orWhere('friendships.recipient_id', auth()->user()->id);
                    });
                });
            });
        } else {
            $user = User::select('users.*')->whereUserName($profileId);
        }
        $user = $user->first();
        if ($user->isStudent()) {
            $data = User::with('role', 'address', 'profile_options', 'ielts.examLevel', 'interests', 'educations.institute', 'educations.educationLevel')->whereUserName($profileId)->first();
        }


        if ($user->isSchool()) {
            $data = User::with('role', 'schoolAddress', 'instituteAuthor.department',
                'instituteAuthor.instituteRole', 'institute.educationLevel', 'videos','institute.programs.educationLevels')
                ->whereUserName($profileId)->first();
        }

        if ($user->isTutor()) {
            $data = User::with('role', 'address', 'experiences.institute', 'educations.institute', 'educations.educationLevel', 'courses.educationLevel', 'courses.licence', 'licences', 'courses.serviceModes')->whereUserName($profileId)->first();
        }

        $data->friend = ($user->fId > 0) ? true : false;
        return $data;
    }

    public function openPublicProfile(Request $request, $profileId)
    {
        $data = User::with('role', 'schoolAddress', 'instituteAuthor.department',
            'instituteAuthor.instituteRole', 'institute.educationLevel', 'videos','institute.programs.educationLevels')->whereUserName($profileId)->first();
        return $data;
    }

    public function publicProfileView(Request $request, $profileId)
    {
        if (Auth::check()) {
            //app('\App\Http\Controllers\HomeController')->index();
            //return redirect(url("profile/$profileId"));
            return view('home');
        }
        return view('web.public-profile', compact('profileId'));
    }

    public function getAllEducationLevels($institute_id)
    {
        $res = DB::table('education_levels')
            ->join('program_education_levels', 'education_levels.id', '=', 'program_education_levels.education_level_id')
            ->where('program_education_levels.institute_id', $institute_id)
            ->select('education_levels.*')
            ->groupBy('education_levels.id')
            ->get();
        return $res;
    }

    public function getProgramsByEducationLevel(Request $request, $institute_id)
    {
        $sql = Program::select('programs.*')->with('educationLevels')
            ->join('program_education_levels','program_education_levels.program_id','=','programs.id')
            ->where('program_education_levels.institute_id', $institute_id);


        if ($request->education_level_id !='') {
            $sql->where('program_education_levels.education_level_id','=',$request->education_level_id);
        }
        if ($request->program_id !='') {
            $sql->where('programs.id', '=', $request->program_id);
        }
        if (isset($request->program_title)) {
            $sql->where('programs.title', 'like', '%' . $request->program_title . '%');
        }
        if (isset($request->sort_by)) {
            $sort = $request->sort_by == 0 ? 'ASC' : 'DESC';
            $sql->orderBy('programs.title', $sort);
        } else {
            $sql->orderBy('programs.admission_date', 'DESC');
        }
        $programs = $sql->get();
        return $programs;
    }

    public function getTopDisciplines(Request $request, $profileId)
    {
        $user = User::whereUserName($profileId)->first();
        $sql = null;
        $records = null;
        if ($user->isSchool()) {
            DB::enableQueryLog();
            $records = DB::table('users')
                ->select('programs.institute_id', 'programs.title', 'programs.degrees')
                ->selectRaw('COUNT(*) as count_programs')
                ->leftjoin('institutes', 'users.id', '=', 'institutes.user_id')
                ->leftjoin('programs', 'institutes.id', '=', 'programs.institute_id')
                ->groupBy('programs.title')
                ->whereUserName($profileId)->get();
            $sql = DB::getQueryLog();
        }
        $data = [];
        $total = 0;
        foreach ($records as $key => $row) {
            $degrees = json_decode($row->degrees);
            if (is_array($degrees)) {
                $total += count(json_decode($row->degrees));
            }
        }
        foreach ($records as $key => $row) {
            $degrees = json_decode($row->degrees);
            $degree = (is_array($degrees))?count($degrees): 1;
            $data[] = array(
                'title' => $row->title,
                'count' => $degree,
                'percentage' => ($total <= 0) ? 0 : $degree / $total * 100,
            );
            if ($key == 9) {
                break;
            }
        }
        if ($records) {
            return response()->json(['success' => true, 'total' => $total, 'data' => $data, 'sql' => $sql], 200);
        } else {
            return response()->json(['success' => false, 'total' => 0, 'data' => 'Whoops! No Data Found.', 'sql' => $sql], 200);
        }
    }
}
