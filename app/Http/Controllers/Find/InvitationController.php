<?php

namespace App\Http\Controllers\Find;

use App\Http\Controllers\Controller;
use App\Notifications\InvitationNotification;
use Hootlex\Friendships\Models\Friendship;
use Hootlex\Friendships\Status;
use Illuminate\Http\Request;
use App\User;

class InvitationController extends Controller
{
    public function sendInvitation(Request $request)
    {
        $model = User::with(['social_connect', 'role'])->find( $request->item );

        $model->message = 'Program Title: ' . $request->program['title'] . ' - '. $request->message;
        $model->module = 'invitation';
        auth()->user()->befriend( $model, $request->message );
        $model->notify( (new InvitationNotification( $model ))->delay( 5 ) );

        return response()->json( $model );
    }

    public function pendingRequestList()
    {
        $recipients = Friendship::with(['recipient.address.country', 'recipient.role'])->where( 'sender_id', auth()->id() )
            ->whereStatus( Status::PENDING )->paginate( 8 );

        return response()->json( $recipients );
    }

    public function invitationPendingList()
    {
        $recipients = Friendship::with(['sender.address.country', 'sender.role'])->where( 'recipient_id', auth()->id() )
            ->whereStatus( Status::PENDING )->paginate( 8 );

        return response()->json( $recipients );
    }

    public function invitationPendingSingle()
    {
        $recipient = Friendship::with(['sender.address.country', 'sender.role'])->where( 'recipient_id', auth()->id() )
            ->whereStatus( Status::PENDING )->latest()->first();

        return response()->json( $recipient );
    }

    public function acceptRequest(Request $request)
    {
        $friend = Friendship::find( $request->id );
        if ($request->status) {
            $friend->status = Status::ACCEPTED;
            $friend->save();

            //For Notification #89
            $model = User::with(['social_connect', 'role'])->find( $friend->sender_id );
            $model->message = auth()->user()->name.' Accepted your friend request.';
            $model->module = 'accept';
            $model->notify( (new InvitationNotification( $model ))->delay( 5 ) );

            return response()->json( ['message' => 'Successfully accept the request'] );
        }
    }

    public function acceptAllRequest()
    {
        $pendingRequests = Friendship::where( 'recipient_id', auth()->id() )->whereStatus( Status::PENDING )->pluck( 'id' );
        $acceptAll = Friendship::whereIn( 'id', $pendingRequests )->update( ['status' => Status::ACCEPTED] );

        return response()->json( ['message' => 'Successfully accept all invitations'] );
    }

    public function ignoreRequest(Request $request)
    {
        $friend = Friendship::find( $request->id );
        $friend->status = Status::DENIED;
        $friend->save();

        //For Notification #89
        $model = User::with(['social_connect', 'role'])->find( $friend->sender_id );
        $model->message = auth()->user()->name.' Ignore your friend request.';
        $model->module = 'ignore';
        $model->notify( (new InvitationNotification( $model ))->delay( 5 ) );

        return response()->json( ['message' => 'Successfully updated'] );
    }

    public function ignoreAllRequest()
    {
        $pendingRequests = Friendship::where( 'recipient_id', auth()->id() )->whereStatus( Status::PENDING )->pluck( 'id' );
        $ignoreAll = Friendship::whereIn( 'id', $pendingRequests )->update( ['status' => Status::DENIED] );

        return response()->json( ['message' => 'Successfully accept the request'] );
    }

    public function cancelRequest(Request $request)
    {
        $friend = Friendship::find( $request->id );
        if ($friend) {
            $friend->delete();
        }

        return response()->json( ['message' => 'Successfully canceled'] );
    }

    public function cancelAllRequest()
    {
        $pendingRequests = Friendship::where( 'recipient_id', auth()->id() )->whereStatus( Status::PENDING )->pluck( 'id' );
        $ignoreAll = Friendship::whereIn( 'id', $pendingRequests )->delete();

        return response()->json( ['message' => 'Successfully accept the request'] );
    }
}
