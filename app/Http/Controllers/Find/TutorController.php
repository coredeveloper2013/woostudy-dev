<?php

namespace App\Http\Controllers\Find;

use App\Country;
use App\Course;
use App\Filters\DataFilter;
use App\Filters\FindTutorFilter;
use App\Notifications\CourseInvitationNotification;
use App\User;
use Hootlex\Friendships\Models\Friendship;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class TutorController extends Controller
{
    public function __construct(FindTutorFilter $filter)
    {
        parent::__construct( $filter );
    }

    public function findCourses()
    {
        $existingInvites = $this->existingInvitations();

        return Course::with( ['user' => function ($q) {
            $q->with( ['address', 'experiences' => function ($q) {
                $q->orderBy( 'from_date' );
            }] );
        }, 'licence'] )->whereNotIn( 'id', $existingInvites )->filter( $this->filter )->paginate( 8 );
    }

    public function findCoursesForStudent()
    {
        $existingInvites = $this->existingInvitations();

        return Course::with( 'user.address' )->whereNotIn( 'id', $existingInvites )->filter( $this->filter )->paginate( 8 );
    }

    public function sendCourseInvitation(Request $request)
    {
        $model = Course::find( $request->item );
        if ($model->user) {
            $model->user->social_connect;

            $model->message = $request->message;
            auth()->user()->befriend( $model, $request->message );
            $model->user->notify( (new CourseInvitationNotification( $model ))->delay( 5 ) );

            return response()->json( ['user' => $model->user, 'course' => $model] );
        }

        return response()->json( ['message' => 'No data found'] );
    }

    private function existingInvitations()
    {
        $recipients = Friendship::where( 'recipient_type', Course::class )->where( 'sender_type', User::class )->where( 'sender_id', auth()->id() )->pluck( 'recipient_id' )->toArray();
        $senders = Friendship::where( 'sender_type', Course::class )->where( 'recipient_type', User::class )->where( 'recipient_id', auth()->id() )->pluck( 'sender_id' )->toArray();

        return array_merge( $recipients, $senders );
    }

    public function sendEmailInvitation(Request $request)
    {
      if (!isset($request->email) || $request->email=='') {
        return response()->json(['success' => false, 'message' =>  auth()->user()], 400);
      }

      $mailMessage = '';
      if (!isset($request->email) || $request->email=='') {
        $mailMessage = $request->mailMessage;
      }
      $emails = explode(',', $request->email);

      \Mail::send('emails.invite', ['user' => auth()->user(), 'mailMessage' => $mailMessage], function ($m) use ($emails) {
          $m->from(env('MAIL_FROM_ADDRESS'), env('MAIL_FROM_NAME'));
          $m->to($emails)->subject(config('app.name', 'Laravel').'Connection Invitation');
      });
      return response()->json(['success' => true, 'message' => 'Email sent!'], 200);
    }
}
