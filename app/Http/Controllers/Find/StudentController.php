<?php

namespace App\Http\Controllers\Find;

use App\Filters\FindStudentFilter;
use App\Http\Controllers\Controller;
use App\Notifications\StudentInvitationNotification;
use App\User;
use Hootlex\Friendships\Models\Friendship;
use Hootlex\Friendships\Status;
use Illuminate\Http\Request;

class StudentController extends Controller
{
    public function __construct(FindStudentFilter $filter)
    {
        parent::__construct( $filter );
    }

    /*public function findStudents()
    {
        return User::whereRoleId( 2 )->with( ['highestEducation', 'profile_options' => function ($query) {
            $query->where( 'option_key', 'date_of_birth' );
        }] )->filter( $this->filter )->paginate( 8 );
    }*/


    public function findStudents()
    {
        $existingInvites = $this->existingInvitations();

        return User::with( ['address', 'profile_options' => function ($q) {
            $q->where( 'option_key', 'date_of_birth' );
        }, 'lastEducation.institute', 'lastIelts'] )->whereRoleId( 2 )->whereNotIn( 'id', $existingInvites )->filter( $this->filter )->paginate( 8 );
    }

    public function findStudentsForStudent()
    {
        $existingInvites = $this->existingInvitations();

        return User::with( 'lastInterest', 'address' )->whereRoleId( 2 )->whereNotIn( 'id', $existingInvites )->filter( $this->filter )->paginate( 8 );
    }

    public function sendStudentInvitation(Request $request)
    {
        $model = User::with('social_connect')->find( $request->item );

        $model->message = $request->message;
        auth()->user()->befriend( $model, $request->message );
        $model->notify( (new StudentInvitationNotification( $model ))->delay( 5 ) );

        return response()->json( ['user' => $model, 'program' => $model] );
    }

    private function existingInvitations()
    {
        $recipients = Friendship::where( 'recipient_type', User::class )->where( 'sender_type', User::class )->where( 'sender_id', auth()->id() )->pluck( 'recipient_id' )->toArray();
        $senders = Friendship::where( 'sender_type', User::class )->where( 'recipient_type', User::class )->where( 'recipient_id', auth()->id() )->pluck( 'sender_id' )->toArray();
        return array_merge( $recipients, $senders );
    }

    public function invitationPendingList()
    {
        $recipients = Friendship::where( 'recipient_id', auth()->id() )->whereStatus( Status::PENDING )->paginate( 8 );

        $recipients->each( function ($value) {
            $field = new $value->sender_type();
            $data = $field->find( $value->sender_id );
            $value->setRelation( 'model', $data );

        } );

        return response()->json( $recipients );
    }

    public function sendEmailInvitation(Request $request)
    {
      if (!isset($request->email) || $request->email=='') {
        return response()->json(['success' => false, 'message' =>  auth()->user()], 400);
      }

      $mailMessage = '';
      if (!isset($request->email) || $request->email=='') {
        $mailMessage = $request->mailMessage;
      }
      $emails = explode(',', $request->email);

      \Mail::send('emails.invite', ['user' => auth()->user(), 'mailMessage' => $mailMessage], function ($m) use ($emails) {
          $m->from(env('MAIL_FROM_ADDRESS'), env('MAIL_FROM_NAME'));
          $m->to($emails)->subject(config('app.name', 'Laravel').'Connection Invitation');
      });
      return response()->json(['success' => true, 'message' => 'Email sent!'], 200);
    }
}
