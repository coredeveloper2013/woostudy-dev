<?php

namespace App\Http\Controllers\Find;

use App\Course;
use App\Filters\FindStudentFilter;
use App\Filters\FindTutorFilter;
use App\Filters\FindInstituteFilter;
use App\SearchLog;
use App\User;
use App\Country;
use Carbon\Carbon;
use Hootlex\Friendships\Models\Friendship;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class FindMongoSqlController extends Controller
{
    public function findStudent(FindStudentFilter $filter)
    {

        \DB::enableQueryLog();
        $student = User::select('users.id')
            ->leftJoin('friendships', function ($join) {
                $join->on('users.id', 'friendships.sender_id');
                $join->orOn('users.id', 'friendships.recipient_id');
                $join->where(function ($q) {
                    $q->where(function ($q) {
                        $q->where('friendships.sender_id', auth()->user()->id)
                            ->orWhere('friendships.recipient_id', auth()->user()->id);
                    });
                });
            })->whereRoleId(2)->whereNull('friendships.id')->where('users.id', '!=', auth()->user()->id)->filter($filter)->pluck('id')->toArray();

        $student = \DB::connection('mongodb')->collection('users')->whereIn('id', $student)->where('role_id', '=', 2)->where('id', '!=', auth()->user()->id)->paginate(8);

        if (!(env('APP_ENV') == 'production')) {
            $custom = collect([
                'SQL' => \DB::getQueryLog(),
            ]);
            $student = $custom->merge($student);
        }
        return $student;
    }

    public function findTutor(FindTutorFilter $filter)
    {
        $tutor = User::select('users.id')
            ->leftJoin('friendships', function ($join) {
                $join->on('users.id', 'friendships.sender_id');
                $join->orOn('users.id', 'friendships.recipient_id');
            })->whereRoleId(4)->whereNull('friendships.id')->where('users.id', '!=', auth()->user()->id)->filter($filter)->paginate(8)->pluck('id')->toArray();

        $tutor = \DB::connection('mongodb')->collection('users')->whereIn('id', $tutor)->where('role_id', '=', 4)->where('id', '!=', auth()->user()->id)->paginate(8);

        return $tutor;
    }

    //public function findInstitute(FindInstituteFilter $filter)
    public function findInstitute(Request $request)
    {
        $freegeoipjson = file_get_contents("http://api.ipstack.com/".$_SERVER['REMOTE_ADDR']."?access_key=fbb1b3d287ab1b4a252eaeda92531e87");

        //SearchLog insert
        $log = SearchLog::create([
            'user_id' => auth()->id(),
            'search_criteria' => json_encode($request->all()),
            'date_time' => date('Y-m-d H:i:s'),
            'geo_location' => isset($freegeoipjson) ? $freegeoipjson : '',
            'ip_address' => $_SERVER['REMOTE_ADDR'],
            'type' => 'school',
        ]);
        //End SearchLog insert

        // $institute = User::select('users.id')
        // ->leftJoin('friendships', function ($join) {
        //     $join->on('users.id', 'friendships.sender_id');
        //     $join->orOn('users.id', 'friendships.recipient_id');
        // })->whereRoleId( 3 )->whereNull('friendships.id')->where('users.id', '!=', auth()->user()->id)->filter( $filter )->paginate(8)->toArray();

        // $institute['data'] = array_column($institute['data'], 'id');
        // $institute['data'] = \DB::connection('mongodb')->collection('users')->whereIn('id', $institute['data'])->get();
        // return $institute;

        $sql = User::select('users.id', 'users.name', 'institutes.title', 'friendships.id AS fid')
            // ->leftJoin('friendships', function ($join) {
            //     $join->on('users.id', 'friendships.sender_id');
            //     $join->orOn('users.id', 'friendships.recipient_id');
            //     $join->where(function ($q) {
            //         $q->where('friendships.sender_id', auth()->user()->id)
            //             ->orWhere('friendships.recipient_id', auth()->user()->id);
            //     });
            // })
            // ->whereNull('friendships.id')

            ->leftJoin('friendships', function ($join) {
                $join->on(function ($q) {
                    $q->on('friendships.sender_id', 'users.id');
                    $q->on('friendships.recipient_id', \DB::raw(auth()->user()->id));
                });
                $join->orOn(function ($q) {
                    $q->on('friendships.recipient_id', 'users.id');
                    $q->on('friendships.sender_id', \DB::raw(auth()->user()->id));
                });
            })
            // ->whereNull('friendships.id')
            ->whereRoleId(3)
            ->where('users.id', '!=', auth()->user()->id)
            ->distinct('users.id');

        $sql->join('institutes', 'users.id', '=', 'institutes.user_id');
        $sql->leftJoin(\DB::raw("(SELECT * FROM programs GROUP BY institute_id) AS programs"), 'institutes.id', '=', 'programs.institute_id');
        $sql->leftJoin('institute_author_user', 'institutes.id', '=', 'institute_author_user.institute_id');
        $sql->leftJoin('addresses', 'users.id', '=', 'addresses.addressable_id');

        if ($request->institute != '') {
            $sql->where('institutes.title', 'Like', '%' . $request->institute . '%');
        }

        if ($request->title != '') {
            $sql->where('programs.title', 'Like', '%' . $request->title . '%');
        }

        if ($request->region > 0) {
            $sql->leftJoin('countries', 'addresses.country_id', '=', 'countries.id');
            $sql->where('countries.region_id', $request->region);
        }

        if ($request->country > 0) {
            $sql->where('addresses.country_id', $request->country);
        }

        if ($request->state > 0) {
            $sql->where('addresses.state_id', $request->state);
        }

        if ($request->locality == 2) {
            $jsondata = json_decode($freegeoipjson);
            $country_name = $jsondata->country_name;
            $country = Country::where('title', $country_name)->first();
            if ($country) {
                $sql->where('addresses.country_id', $country->id);
            }
        }


        if ($request->distance != '') {
            $radius = (double)$request->distance;
            $lat = (float)$request->lat;
            $lng = (float)$request->lng;
            $unit = 6378.10;
            $sql->join(\DB::raw("(select `addressable_id`, (6378.1 * acos(cos(radians($lat)) * cos(radians(lat)) * cos(radians(lng) - radians($lng)) + sin(radians($lat)) * sin(radians(lat)))) AS distance FROM addresses ORDER BY distance ASC) AS B"), function ($q) use ($request) {
                $q->where('B.distance', '<=', $request->distance);
                $q->on('users.id', '=', 'B.addressable_id');
            });
        }

        if ($request->range != '') {
            $fees = explode(',', $request->range);
            $sql->whereBetween('programs.fee_int', $fees);

            // $fees = array_filter($fees);
            // foreach ($fees as $key => $fe) {
            //   $fe = preg_replace('/[^0-9.]+/', '', $fe);
            //   $newFee[] = ($fe>0)?$fe:0;
            // }
            // if (!empty($newFee) && count($newFee)==1) {
            //     $newFee = [
            //         0 => 0,
            //         1 => preg_replace('/[^0-9.]+/', '', $newFee[0]),
            //     ];
            // } else {
            //   $newFee = [
            //         0 => preg_replace('/[^0-9.]+/', '', $newFee[0]),
            //         1 => preg_replace('/[^0-9.]+/', '', $newFee[1]),
            //     ];
            // }
            // $sql->whereBetween('programs.fee_int', $newFee);
        }

        if ($request->department != '') {
            $sql->where('institute_author_user.department_id', $request->department);
        }

        if ($request->role != '') {
            $sql->where('institute_author_user.institute_role_id', $request->role);
        }

        $institute = $sql->orderBy('institutes.title', "ASC")->paginate(8);
        $instituteIds = $institute->pluck('id')->toArray();;
        $mongoData = \DB::connection('mongodb')->collection('users')->whereIn('id', $instituteIds)->orderBy('institute.title', "ASC")->get();

        $friendCheck = [];
        foreach ($institute as $f) {
            $friendCheck[$f->id] = $f->fid;
        }

        $dataArr = [];
        foreach ($mongoData as $data) {
            $dataArr[] = [
                'friendship' => $friendCheck[$data['id']],
                'id' => $data['id'],
                'user_name' => $data['user_name'],
                'name' => $data['name'],
                'avater' => $data['avater'],
                'address' => $data['address'],
                'institute' => [
                    'title' => $data['institute']['title'],
                    'programs' => $data['institute']['programs'],
                ]
            ];
        }
        $institute = $institute->toArray();
        $institute['data'] = $dataArr;
        return $institute;
    }

    public function mutuals(Request $request)
    {
        $user = User::find(auth()->id());
        $fr = $user->getFriendsOfFriends()->toArray();
        $frIds = array_column($fr, 'id');

        $input = [];
        $input['name'] = isset($request->name) ? $request->name : '';
        $page = isset($request->page) ? $request->page : 1;
        $input['region'] = isset($request->region) ? $request->region : 0;
        $input['common'] = isset($request->common) ? $request->common : '';
        $input['lat'] = isset($request->common) ? $request->common : 0;
        $input['gender'] = isset($request->gender) ? $request->gender : '';
        $input['key'] = isset($request->key) ? $request->key : '';
        $input['school'] = isset($request->school) ? $request->school : '';
        $input['distance'] = isset($request->distance) ? $request->distance : '';

        $users = User::select('users.id')
            ->leftjoin('addresses', 'addresses.addressable_id', 'users.id')
            ->leftjoin('countries', 'countries.id', 'addresses.country_id')
            ->leftjoin('institutes', 'institutes.user_id', 'users.id')
            ->leftjoin('profile_options', 'profile_options.user_id', 'users.id')
            ->leftjoin('interests', 'interests.user_id', 'users.id')
            ->where(function ($query) use ($input) {
                if ($input['name'] != '') {
                    $query->where('users.name', 'Like', '%' . $input['name'] . '%');
                }

                if ($input['region'] != 0) {
                    $query->where('countries.region_id', $input['region']);
                }

                if ($input['school'] != '') {
                    $query->where('institutes.title', 'LIKE', "%" . $input['school'] . "%");
                }

                if ($input['gender'] != '') {
                    $query->where([
                        'profile_options.option_key' => 'gender',
                        'profile_options.option_value' => $input['gender']
                    ]);
                }
                if ($input['key'] != '') {
                    $query->where([
                        'users.name' => '%' . $input['key'] . '%',
                        'institutes.title' => '%' . $input['key'] . '%',
                    ]);
                }

                if ($input['common'] != '') {
                    $query->where('interests.title', 'LIKE', "%" . $input['common'] . "%");
                }

            })->whereIn('users.id', $frIds)->distinct('users.id')->paginate(8)->pluck('id')->toArray();

        $mutuals = \DB::connection('mongodb')->collection('users')->whereIn('id', $frIds)->where('id', '!=', auth()->user()->id)->whereIn('id', $users)->paginate(8);

        return $mutuals;
    }

    public function findRandomStudents(Request $request)
    {
        $students = User::select('users.*')
            ->where('role_id', 2)
            ->leftJoin('friendships', function ($join) {
                $join->on('users.id', 'friendships.sender_id');
                $join->orOn('users.id', 'friendships.recipient_id');
                $join->where(function ($q) {
                    $q->where('friendships.sender_id', auth()->user()->id)
                        ->orWhere('friendships.recipient_id', auth()->user()->id);
                });
            })
            ->whereNull('friendships.id')
            ->get()->random(6);
        if ($students->count() > 0) {
            return response()->json(['success' => true, 'data' => $students, 'status' => 200], 200);
        } else {
            return response()->json(['success' => false, 'data' => 'No Students Found!', 'status' => 200], 200);
        }
    }

    public function allOnlineCourseUrls()
    {
        return Course::selectRaw('online_site_name as site_name')->where('online_course', 1)->whereNotNull('online_url')->groupBy('online_site_name')->get();
    }

    public function findOnlineCourses(Request $request)
    {
        $sql = Course::select('courses.*', 'friendships.id AS fId')->with(['user.address'])->where('courses.online_course', 1)
            ->where('courses.user_id', '!=', auth()->user()->id)
            ->join('users', 'courses.user_id', '=', 'users.id')
            ->leftJoin('friendships', function ($join) {
                $join->on(function ($q) {
                    $q->on('users.id', 'friendships.sender_id');
                    $q->orOn('users.id', 'friendships.recipient_id');
                });
                $join->where(function ($q) {
                    $q->where('friendships.sender_id', auth()->user()->id)
                        ->orWhere('friendships.recipient_id', auth()->user()->id);
                });
            });

        if ($request->title != '') {
            $sql->where('courses.title', 'Like', '%' . $request->title . '%');
        }

        if ($request->site_name != '') {
            $sql->where('courses.online_site_name', 'Like', '%' . $request->site_name . '%');
        }

        $courses = $sql->orderBy('courses.title', "ASC")->paginate(8)->toArray();
        return $courses;
    }

    public function findCourseById(Request $request)
    {
        return Course::with(['educationLevel', 'licence'])->find($request->course_id);
    }
}
