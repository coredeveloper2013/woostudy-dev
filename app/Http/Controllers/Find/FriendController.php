<?php

namespace App\Http\Controllers\Find;

use App\User;
use Hootlex\Friendships\Models\Friendship;
use Hootlex\Friendships\Status;
use App\Http\Controllers\Controller;

class FriendController extends Controller
{
    public function index()
    {
        $friends = $this->friendLists();
        $users = User::whereIn( 'id', $friends )->paginate();

        return response()->json( $users );
    }

    public function myFriends()
    {
        $friends = $this->friendLists();
        $users = User::with(['address', 'role' => function ($q) {
            $q->select('id', 'title');
        }])->whereIn( 'id', $friends )->paginate();

        return response()->json( $users );
    }

    public function unfriend($friend_id)
    {
        $friend = Friendship::where(function ($q) use ($friend_id) {
            $q->where('sender_id', $friend_id);
            $q->where('recipient_id', auth()->user()->id);
        })
            ->orWhere(function ($q) use ($friend_id) {
                $q->where('recipient_id', $friend_id);
                $q->where('sender_id', auth()->user()->id);
            })->whereStatus(Status::ACCEPTED)->first();
        if ($friend) {
            $friend->status = Status::DENIED;
            $friend->save();
        }
        return response()->json(['statusCode'=>200,'data'=>$friend]  );
    }

    public function count()
    {
        $friends = $this->friendLists();

        $users = count( $friends );

        return response()->json( $users );
    }

    public function friendLists()
    {
        $recipients = Friendship::where( function ($q) {
                $q->where( 'sender_id', auth()->id() )
                ->orWhere( 'recipient_id', auth()->id() );
            } )->where( 'status', Status::ACCEPTED )->get();

        $recipient = $recipients->pluck( 'sender_id' )->unique()->toArray();
        $recipient2 = $recipients->pluck( 'recipient_id' )->unique()->toArray();

        $recipients = array_diff( array_unique( array_merge( $recipient, $recipient2 ) ), [auth()->id()] );

        return $recipients;
    }
}
