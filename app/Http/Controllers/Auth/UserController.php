<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
    public function changPassword(Request $request)
    {
        $request->validate( [
            'current_password' => 'required',
            'password' => 'required|string|min:6|confirmed',
        ] );

        if (!(Hash::check( $request->get( 'current_password' ), Auth::user()->password ))) {
            return response(["errors" => "Your current password does not matches with the password you provided. Please try again."], 404  );
        }

        if (strcmp( $request->get( 'current_password' ), $request->get( 'password' ) ) == 0) {
            return response(["errors" => "New Password cannot be same as your current password. Please choose a different password." ], 404 );
        }
        $user = Auth::user();
        $user->password = bcrypt( $request->get( 'password' ) );
        $user->save();

        return response()->json( ['success' => 'Password changed successfully !'] );

    }
}
