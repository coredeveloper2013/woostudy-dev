<?php

namespace App;


class Upgrade extends Models
{
    protected $table = 'upgrades';
    protected $fillable = ['user_id', 'payment_type', 'payment_date', 'card_number', 'card_full_name', 'card_expire', 'card_cvc', 'amount', 'payment_info'];
}
