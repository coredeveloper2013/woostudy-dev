<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Chatting extends Model
{
    protected $table = 'chattings';
    protected $fillable = ['from', 'to', 'read_at', 'message'];

    public function from()
    {
        return $this->belongsTo( User::class, 'from' );
    }

    public function to()
    {
        return $this->belongsTo( User::class, 'to' );
    }
}
