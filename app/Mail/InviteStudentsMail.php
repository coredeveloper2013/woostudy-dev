<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class InviteStudentsMail extends Mailable
{
    use Queueable, SerializesModels;

    public $school;
    public $students;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($school, $students)
    {
        $this->school = $school;
        $this->students = $students;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->markdown('emails.schoolEmail');
    }
}
