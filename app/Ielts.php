<?php

namespace App;


class Ielts extends Models
{
    protected $fillable = ['taken_on', 'score', 'grade', 'user_id', 'ielts_exam_level_id'];

    //Save in MongoDB
    protected static function boot() {
        parent::boot();
        static::created( function () {
            $data = \App\User::mongoSave();
        });

        static::updated( function () {
            $data = \App\User::mongoSave();
        });

        static::deleted( function () {
            $data = \App\User::mongoSave();
        });
    }

    public function user()
    {
        return $this->belongsTo( User::class );
    }

    public function examLevel()
    {
        return $this->belongsTo( IELTSExamLevel::class , 'ielts_exam_level_id');
    }

}
