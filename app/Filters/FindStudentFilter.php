<?php

namespace App\Filters;


class FindStudentFilter extends FindInvitationFilter
{

    public function gpa($value)
    {
        return $this->builder->whereHas('educations', function ($qury) use ($value) {
            $qury->whereGpa($value);
        });
    }

    public function score($value)
    {
        return $this->builder->whereHas('educations', function ($qury) use ($value) {
            $qury->wherePercentage($value);
        });
    }

    public function program($value)
    {
        return $this->builder->whereHas('educations', function ($qury) use ($value) {
            $qury->where('title', 'like', '%' . $value . '%');
        });
    }

    public function grade($value)
    {
        return  $this->builder->whereHas( 'educations', function ($qury) use ($value) {
            $qury->whereGrade( $value );
        });
    }
}
