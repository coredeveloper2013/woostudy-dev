<?php

namespace App;


class SocialProvider extends Models
{
    protected $fillable = ['provider_id','provider','token', 'refresh_token', 'token_expire','token_secret'];

    public function user(){
        return $this->belongsTo(User::class);
    }
}
