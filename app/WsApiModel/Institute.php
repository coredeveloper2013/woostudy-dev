<?php

namespace App\WsApiModel;
use Illuminate\Database\Eloquent\Model;


class Institute extends Model
{
    public function programs()
    {
        return $this->hasMany( Program::class )->orderBy('admission_date', 'DESC');
    }
}
