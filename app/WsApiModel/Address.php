<?php

namespace App\WsApiModel;
use Illuminate\Database\Eloquent\Model;


class Address extends Model
{
    protected $table = 'addresses';

    public function country()
    {
        return $this->belongsTo( 'App\Country' );
    }

    public function state()
    {
        return $this->belongsTo( 'App\State' );
    }

    public function programs()
    {
        return $this->belongsToMany(Program::class, 'program_branches', 'address_id', 'program_id')->withPivot('institute_id')->withTimestamps();
    }
}
