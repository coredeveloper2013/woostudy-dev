<?php

namespace App\WsApiModel;

use App\Course;
use App\Education;
use App\Experience;
use App\Ielts;
use App\Interest;
use App\ProfileOption;
use Illuminate\Database\Eloquent\Model;

class User extends Model
{
    public static function mongoSaveApi($role, $id)
    {
        if ($role == 2) {
            $data = User::with( ['address.country.region', 'educations.institute', 'ielts', 'interests', 'profile_options'])->find($id)->toArray();
            $data ['avater'] = '/storage/uploads/images/avatar.jpg';
        } elseif ($role == 3) {
            $data = User::with( ['address.country.region', 'institute.programs.educationLevels', 'institute.programs.branches', 'courses.licence', 'instituteAuthor'])->find($id)->toArray();
            $data ['avater'] = '/storage/uploads/images/school.png';
        } elseif ($role == 4) {
            $data = User::with( ['address.country.region', 'educations.institute', 'courses.licence', 'experiences', 'profile_options'])->find($id)->toArray();
            $data ['avater'] = '/storage/uploads/images/tutor.png';
        }

        return $insertData = \DB::connection('mongodb')->collection('users')->where('id', (int)$id)->update($data , ['upsert' => true]);
    }

    // public static function mongoSaveApi($role, $id)
    // {
    //     if ($role ==2) {
    //         $data = User::with( ['address.country.region', 'educations.institute', 'ielts', 'interests', 'profile_options'])->find($id)->toArray();
    //         $data ['avater'] = '/storage/uploads/images/avatar.jpg';
    //     } elseif ($role ==3) {
    //         $data = User::with( ['address.country.region', 'institute.programs', 'courses.licence', 'instituteAuthor'])->find($id)->toArray();
    //         $data ['avater'] = '/storage/uploads/images/school.png';
    //     } elseif ($role ==4) {
    //         $data = User::with( ['address.country.region', 'educations.institute', 'courses.licence', 'experiences', 'profile_options'])->find($id)->toArray();
    //         $data ['avater'] = '/storage/uploads/images/tutor.png';
    //     }

    //     $insertData = \DB::connection('mongodb')->collection('users')->where('id', $id)->update($data , ['upsert' => true]);
    // }

    public static function mongoDeleteApi($ids)
    {
        \DB::connection('mongodb')->collection('users')->whereIn('id', $ids)->drop();
    }

    public function instituteAuthor()
    {
        return $this->hasOne( InstituteAuthor::class, 'created_by' );
    }

    public function institute()
    {
        return $this->hasOne( Institute::class, 'user_id' );
    }

    public function address()
    {
        return $this->hasOne( Address::class, 'addressable_id' );
    }

    public function educations()
    {
        return $this->hasMany( Education::class )->orderBy('completion_date', 'DESC');
    }

    public function ielts()
    {
        return $this->hasMany( Ielts::class );
    }

    public function interests()
    {
        return $this->hasMany( Interest::class )->orderBy('since', 'DESC');
    }

    public function profile_options()
    {
        return $this->hasMany( ProfileOption::class);
    }

    public function courses()
    {
        return $this->hasMany( Course::class )->orderBy('starting_date', 'DESC');
    }

    public function experiences()
    {
        return $this->hasMany( Experience::class )->orderBy('to_date', 'DESC');
    }
}
