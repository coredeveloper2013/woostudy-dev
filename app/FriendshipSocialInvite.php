<?php

namespace App;

class FriendshipSocialInvite extends Models
{
    protected $fillable = ['user_id', 'custom_message', 'short_url'];

    public function user()
    {
        return $this->belongsTo( User::class );
    }
}
