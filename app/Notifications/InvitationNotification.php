<?php

namespace App\Notifications;

use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Notifications\Notification;
use Illuminate\Notifications\Messages\MailMessage;

class InvitationNotification extends Notification implements ShouldBroadcast
{
    use Queueable;

    private $user;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($user)
    {
        $this->user = $user;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail', 'database', 'broadcast'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
            ->line( $this->user->message ?? ' A '. auth()->user()->role->title .' ' . auth()->user()->name . ' want to join with you ' . $this->user->name )
            ->action( 'User details', url( '/public/profile/'.auth()->user()->user_name ) );
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            'message' => $this->user->message ?? 'Want to join with you ',
            'to'=>$notifiable,
            'from'=>auth()->user(),
            'module'=> $this->user->module ?? 'invitation',
        ];
    }

    public function broadcastOn()
    {
        return new PrivateChannel( 'App.User.' . $this->user->id );
    }
}
