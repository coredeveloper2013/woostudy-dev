<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Notifications\Notification;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Notifications\Messages\MailMessage;

class ProgramInvitationNotification extends Notification implements ShouldBroadcast
{
    use Queueable;

    private $program;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($program)
    {
        $this->program = $program;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail', 'database','broadcast'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
            ->line( $this->program->message ?? ' A user ' . auth()->user()->name . ' want to join your program ' . $this->program->title )
            ->action( 'User details', url( '/program/details' ) );
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            'message' => $this->program->message ?? 'Want to connect your program ' . $this->program->title,
            'to'=>$notifiable,
            'from'=>auth()->user(),
            'module'=>'program',
        ];
    }

    public function broadcastOn()
    {
        return new PrivateChannel( 'App.User.' . $this->program->institute->user->id );
    }
}
