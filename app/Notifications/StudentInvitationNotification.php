<?php

namespace App\Notifications;

use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Notifications\Notification;
use Illuminate\Notifications\Messages\MailMessage;

class StudentInvitationNotification extends Notification implements ShouldBroadcast
{
    use Queueable;

    private $student;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($student)
    {
        $this->student = $student;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail', 'database', 'broadcast'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
            ->line( $this->student->message ?? ' A user ' . auth()->user()->name . ' want to join with you ' . $this->student->name )
            ->action( 'User details', url( '/student/details' ) );
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            'message' => $this->student->message ?? 'Want to join with you ',
            'to'=>$notifiable,
            'from'=>auth()->user(),
            'module'=>'student',
        ];
    }

    public function broadcastOn()
    {
        return new PrivateChannel( 'App.User.' . $this->student->id );
    }
}
