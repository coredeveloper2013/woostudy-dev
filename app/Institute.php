<?php

namespace App;


class Institute extends Models
{
    protected $fillable = ['title', 'education_level_id', 'user_id', 'email', 'phone',
      'founded',
      'total_students',
      'about',
      'cost_of_tuition',
      'cost_of_living',
      'application_fee',
      'estimated_total',
      'created_by',
      'old_id'];

    //Save in MongoDB
    protected static function boot() {
        parent::boot();
        static::created( function () {
            $data = \App\User::mongoSave();
        });

        static::updated( function () {
            $data = \App\User::mongoSave();
        });

        static::deleted( function () {
            $data = \App\User::mongoSave();
        });
    }

    public function educations()
    {
        return $this->hasMany( Education::class );
    }

    public function author()
    {
        return $this->hasOne( InstituteAuthor::class );
    }

    public function address()
    {
        return $this->morphMany( Address::class, 'addressable' );
    }

    public function user()
    {
        return $this->belongsTo( User::class );

    }

    public function educationLevel()
    {
        return $this->belongsTo( EducationLevel::class );

    }

    public function programs()
    {
        return $this->hasMany( Program::class );
    }
}
