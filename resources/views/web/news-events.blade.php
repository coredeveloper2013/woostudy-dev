@extends('layouts.web')

@section('content')
<section class="works">
  <div class="w-text">News and Events</div>
</section>

<section class="about-feature">
  <div id="feature-1" class="container">
      <div class="row">
          <div class="col-md-12">
              <div class="text-justify about-term">
                <p>Woostudy.com provides the platform for teachers, institutions, and students to keep in touch with each other. 
                The major concern is to change your educational perspective into the modern way and also connect them via social media.</p>
                <p>Connect with woostudy and get notified with the latest education news and events around the world.</p>
              </div>
          </div>
      </div>
  </div>
</section>
@endsection
