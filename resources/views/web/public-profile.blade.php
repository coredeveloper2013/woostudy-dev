@extends('layouts.app')

@section('content')
    <public-profile profile_slug="{{ $profileId }}"></public-profile>
@endsection
