@extends('layouts.web')

@section('content')
<section class="works">
  <div class="w-text">About Us</div>
</section>

<section class="about-feature">
  <div id="feature-1" class="container">
      <div class="row">
          <div class="col-md-12">
              <div class="text-justify about-term">WooStudy has been recognized as the leader in comprehensive international recruitment solutions. We use the latest tools and technology to connect colleges and universities in North America with qualified students from around the world who are eager to expand their academic horizons within the American education system.
              </div>
              <div class="text-area privacy mb-4">
                  <div class="text-no privacy font-white bg-ash">1</div>
                  <div class="text-detail">
                      <div class="text-2-privacy">With WooStudy at your side, we lay the groundwork for you to stand out in the competitive international student recruitment marketplace, to navigate dips in enrollment, and to weather fluctuations in the social and political climate you are challenged with today.
                      </div>
                  </div>
              </div>
              <div class="text-area privacy mb-4">
                  <div class="text-no privacy font-white bg-ash">2</div>
                  <div class="text-detail">
                      <div class="text-2-privacy">We understand the value in growing your international student population, as well as the recruitment challenges you face, and go the extra mile to support you with smart, tried-and-true strategies refined over four decades.                       </div>
                  </div>
              </div>
             </div>
      </div>
  </div>
</section>
@endsection
