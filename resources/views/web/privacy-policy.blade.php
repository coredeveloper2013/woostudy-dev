@extends('layouts.web')

@section('content')
<section class="works">
  <div class="w-text">Our Privacy Policy</div>
</section>

<section class="about-feature">
  <div id="feature-1" class="container">
      <div class="row">
          <div class="col-md-12">
              <div class="text-justify about-term">Your privacy is very important to us. Accordingly, we have
                  developed this Policy in order for you to understand how we collect, use, communicate and disclose
                  and make use of personal information. The following outlines our privacy policy.
              </div>
              <div class="text-area privacy mb-4">
                  <div class="text-no privacy font-white bg-ash">1</div>
                  <div class="text-detail">
                      <div class="text-2-privacy">Before or at the time of collecting personal information, we will
                          identify the purposes for which information is being collected.
                      </div>
                  </div>
              </div>
              <div class="text-area privacy mb-4">
                  <div class="text-no privacy font-white bg-ash">2</div>
                  <div class="text-detail">
                      <div class="text-2-privacy">We will collect and use of personal information solely with the
                          objective of fulfilling those purposes specified by us and for other compatible purposes,
                          unless we obtain the consent of the individual concerned or as required by law.
                      </div>
                  </div>
              </div>
              <div class="text-area privacy mb-4">
                  <div class="text-no privacy font-white bg-ash">3</div>
                  <div class="text-detail">
                      <div class="text-2-privacy">We will only retain personal information as long as necessary for
                          the fulfillment of those purposes.
                      </div>
                  </div>
              </div>
              <div class="text-area privacy mb-4">
                  <div class="text-no privacy font-white bg-ash">4</div>
                  <div class="text-detail">
                      <div class="text-2-privacy">We will collect personal information by lawful and fair means and,
                          where appropriate, with the knowledge or consent of the individual concerned.
                      </div>
                  </div>
              </div>
              <div class="text-area privacy mb-4">
                  <div class="text-no privacy font-white bg-ash">5</div>
                  <div class="text-detail">
                      <div class="text-2-privacy">Personal data should be relevant to the purposes for which it is to
                          be used, and, to the extent necessary for those purposes, should be accurate, complete, and
                          up-to-date.
                      </div>
                  </div>
              </div>
              <div class="text-area privacy mb-4">
                  <div class="text-no privacy font-white bg-ash">6</div>
                  <div class="text-detail">
                      <div class="text-2-privacy">We will protect personal information by reasonable security
                          safeguards against loss or theft, as well as unauthorized access, disclosure, copying, use
                          or modification.
                      </div>
                  </div>
              </div>
              <div class="text-area privacy mb-4">
                  <div class="text-no privacy font-white bg-ash">7</div>
                  <div class="text-detail">
                      <div class="text-2-privacy">We will make readily available to customers information about our
                          policies and practices relating to the management of personal information.
                      </div>
                  </div>
              </div>
              <div class="text-justify about-term">We are committed to conducting our business in accordance with
                  these principles in order to ensure that the confidentiality of personal information is protected
                  and maintained.
              </div>
          </div>
      </div>
  </div>
</section>
@endsection
