@extends('layouts.web')

@section('content')
<section class="works">
  <div class="w-text">Contact Us</div>
</section>

<section class="about-feature">
  <div id="feature-1" class="container">
      <div class="row">
          <div class="col-md-12">
					<div class="card card-body footer-form-fields">
                                          <input type="hidden" value="{{csrf_token()}}">
                                          <h2 style="text-transform:uppercase">Contact  <strong> Us:</strong> </h2>
                                          <input type="text" class="form-control" name="contact_name" id="contact_name" placeholder="Your name">
                                          <input type="emali" class="form-control" name="contact_email" id="contact_email" placeholder="Your email">
                                          <textarea cols="30" rows="5" class="form-control" name="contact_message" id="contact_message" placeholder="Message"></textarea>
                                          <button onclick="sendMail()" class="btn btn-lg btn-warning" style="cursor:pointer;" id="contact_btn">SEND</button>
                                        </div>

          </div>
      </div>
  </div>
</section>
@endsection
@stack('js_stack')
<script>
    function sendMail() {
        var name = $('#contact_name').val();
        if(name=='') {
            toastr.error('Name is required!');
            return false;
        }

        var email = $('#contact_email').val();
        if(email=='') {
            toastr.error('Email is required!');
            return false;
        }

        var message = $('#contact_message').val();
        if(message=='') {
            toastr.error('Message is required!');
            return false;
        }
        $('#contact_btn').html('sending...');

        $.ajax({
            type: 'POST',
            url: "{{route('contact-send')}}",
            data: {'name': name, 'email': email, 'message': message},
            dataType: "json",
            headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            success: function(response) {
                if (response.success) {
                    toastr.success(response.message);
                    $('#contact_name').val('');
                    $('#contact_email').val('');
                    $('#contact_message').val('');
                    $('#contact_btn').html('SEND');
                } else {
                    toastr.error(response.message);
                }
            },
            error: function (xhr, ajaxOptions, thrownError) {
              $('#contact_btn').html('SEND');
              toastr.error(xhr.status+' : '+thrownError);
            }
        });
    }
</script>