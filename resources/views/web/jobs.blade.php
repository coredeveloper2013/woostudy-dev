@extends('layouts.web')

@section('content')
<section class="works">
  <div class="w-text">Jobs</div>
</section>

<section class="about-feature">
  <div id="feature-1" class="container">
      <div class="row">
          <div class="col-md-12">
              <div class="text-justify about-term">
              Start your career at woostudy.com and start earning from today. 
              <p>For Job opportunities please send your resume at careers@woostudy.com</p>
            </div>
      </div>
  </div>
</section>
@endsection
