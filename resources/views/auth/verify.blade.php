@extends('layouts.app')

@section('content')
<div class="auth-page">
    <div class="row">
        <div class="col-lg-8 image"></div>
        <div class="col-lg-4 authContent">
            <div class="authBox">
                <h3 class="text-center">{{ __('Verify Your Email Address') }}</h3>

                @if (session('resent'))
                    <div class="alert alert-success" role="alert">
                        {{ __('A fresh verification link has been sent to your email address.') }}
                    </div>
                @endif

                {{ __('Before proceeding, please check your email for a verification link.') }}
                {{ __('If you did not receive the email') }}, <a href="{{ route('verification.resend') }}">{{ __('click here to request another') }}</a>.
            </div>
        </div>
    </div>
</div>
@endsection
