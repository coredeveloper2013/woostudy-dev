<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="csrf-token" content="{{ csrf_token() }}">
  <title>{{ config('app.name', 'Laravel') }}</title>
  <link href="{{ asset('css/app.css') }}" rel="stylesheet">
  <link rel="stylesheet" href="https://owlcarousel2.github.io/OwlCarousel2/assets/owlcarousel/assets/owl.carousel.min.css">
  <link rel="stylesheet" href="https://owlcarousel2.github.io/OwlCarousel2/assets/owlcarousel/assets/owl.theme.default.min.css">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.css"/>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/socket.io/2.2.0/socket.io.js"></script>
  @stack('css_stack')
  @auth
    <script type="text/javascript" src="https://js.stripe.com/v2/"></script>
    <script>
      <?php
        $authData = auth()->user()->toArray();
        unset($authData['session']);
      ?>
      window.auth = JSON.parse('<?= json_encode($authData); ?>');
      window.SOCKET_NAME = '{{env('SOCKET_NAME')}}';
      window.SOCKET_PROTOCOL = '{{env('SOCKET_PROTOCOL')}}';
      window.FACEBOOK_ID = '{{ env('FACEBOOK_ID') }}';
      window.APP_URL = '{{ env('APP_URL') }}';
      window.FILE_PATH = '{{ asset(env('FILE_PATH')) }}';
      window.STRIPE_KEY = '{{env('STRIPE_KEY')}}';
      //console.log = function(){};
    </script>
  @endauth
</head>
<body data-app_domain={{request()->root()}}>

<div id="app">
  <!--[if lt IE 8]>
  <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please
    <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
  <![endif]-->

  <!-- Top Header -->
  <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
    @csrf
  </form>
  @guest
    <div class="header_top clearfix">
      <div class="container sm-container">
        <div class="row">
          <div class="col-md-12">
            <div class="header_top_left float-xl-left float-lg-left float-md-none float-sm-none float-xs-none">
              <ul>
                <li><a target="_blank" href="{{ env('SOCIAL_FACEBOOK') }}"><i class="fab fa-facebook-f"></i></a></li>
                <li><a target="_blank" href="{{ env('SOCIAL_PINTEREST') }}"><i class="fab fa-pinterest-p"></i></a></li>
                <li><a target="_blank" href="{{ env('SOCIAL_INSTAGRAM') }}"><i class="fab fa-instagram"></i></a></li>
                <li><a target="_blank" href="{{ env('SOCIAL_LINKEDIN') }}"><i class="fab fa-linkedin-in"></i></a></li>
                <li><a target="_blank" href="{{ env('SOCIAL_YOUTUBE') }}"><i class="fab fa-youtube"></i></a></li>
                <li><a target="_blank" href="{{ env('SOCIAL_TWITTER') }}"><i class="fab fa-twitter"></i></a></li>
              </ul>
            </div>
            <div class="header_top_right float-xl-right float-lg-right float-md-none float-sm-none float-xs-none">
              <div class="header_top_right_nav">
                <ul class="nav">
                  <li class="nav-item">
                    <a href="tel:+16465996079" class="nav-link"> <i class="fa fa-phone" aria-hidden="true"></i> +1 (646)
                      599-6079</a>
                  </li>
                  <li class="nav-item">
                    <a href="mailto:admin@woostudy.com" class="nav-link"> <i class="fa fa-envelope"
                                                                             aria-hidden="true"></i> admin@woostudy.com</a>
                  </li>
                </ul>
              </div>
            </div>
          </div>

        </div>
      </div>
    </div>

    <header>
      <nav class="navbar navbar-expand-lg navbar-light  static-top header_area"
           style="background-color: #f3f2ed !important;">
        <div class="container header_area_container">
          <a href="{{ url('/') }}" class="navbar-brand">
            <img src="{{asset('images/logo.png')}}" alt="">
          </a>

          <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive"
                  aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
          </button>

          <div class="collapse navbar-collapse" id="navbarResponsive">
            <ul class="navbar-nav">
              <li class="nav-item active">
                <a class="nav-link" href="{{ url('/') }}">Home</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="{{ env('URL_BLOG') }}">Blog</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="{{ env('URL_ABOUT') }}">About Us</a>
              </li>
            </ul>
          </div>
        </div>
      </nav>
    </header>
  @endguest

  @yield('content')
  <footer class="footer_area">
    <div class="subscribe text-center">
      <div class="s-text mb-2">Stay <strong>Connected</strong></div>
      <div class="s-text-2 text-center">Subscribe for our newsletter for getting latest updates and offers</div>
      <div class="input-group display-inline position-relative">
        <input type="text" class="custom-input" name="subscribe_email" id="subscribe_email" placeholder="Your Email">
        <div class="input-group-prepend">
          <span class="input-group-text" id="subscribe_btn" onclick="subscribe()" id="subscribe_btn">Subscribe</span>
        </div>
      </div>
    </div>

    <section class="footer-nav">
      <div class="container">
        <div class="row">
          <div class="col-md-3 col-sm-6">
            <ul class="list-unstyled">
              <li>
                <div class="head-text">About US</div>
                <div class="horizontal-line"></div>
              </li>
              <li><a href="{{ env('URL_SERVICES') }}">Services</a></li>
              <li><a href="{{ env('URL_JOBS') }}">Jobs</a></li>
              <li><a href="{{ env('URL_BLOG') }}">Blog</a></li>
            </ul>
          </div>
          <div class="col-md-3 col-sm-6">
            <ul class="list-unstyled">
              <li>
                <div class="head-text">Students</div>
                <div class="horizontal-line"></div>
              </li>
              <li><a href="{{ env('URL_ABOUT') }}">About</a></li>
              <li><a href="{{ env('URL_NEWS_EVENT') }}">News and Events</a></li>
              <li><a href="{{ env('URL_SUPPORT') }}">Support</a></li>
              <li><a href="{{ env('URL_FAQ') }}">FAQs</a></li>
            </ul>
          </div>
          <div class="col-md-3  col-sm-6">
            <ul class="list-unstyled">
              <li>
                <div class="head-text">Academics</div>
                <div class="horizontal-line"></div>
              </li>
              <li><a href="{{ url('privacy-policy') }}">Privacy Policy</a></li>
              <li><a href="{{ env('URL_HOW_WORK') }}">How it Works</a></li>
            </ul>
          </div>
          <div class="col-md-3  col-sm-6">
            <div class="head-text pb-2 pt-2">Social</div>
            <div class="horizontal-line mb-3"></div>
            <a class="social" target="_blank" href="{{ env('SOCIAL_FACEBOOK') }}"><i class="fab fa-facebook-f"></i></a>
            <a class="social" target="_blank" href="{{ env('SOCIAL_YOUTUBE') }}"><i class="fab fa-youtube"></i></a>
            <a class="social" target="_blank" href="{{ env('SOCIAL_INSTAGRAM') }}"><i class="fab fa-instagram"></i></a>
            <a class="social" target="_blank" href="{{ env('SOCIAL_PINTEREST') }}"><i class="fab fa-pinterest-p"></i></a>
            <a class="social" target="_blank" href="{{ env('SOCIAL_LINKEDIN') }}"><i class="fab fa-linkedin"></i></a>
            <a class="social" target="_blank" href="{{ env('SOCIAL_TWITTER') }}"><i class="fab fa-twitter"></i></a>
          </div>
        </div>
      </div>
    </section>

    <div class="divider"></div>

    <div class="footer">
      <div class="container">
        <div class="row">
          <div class="col-md-6">
            <div class="footer-logo">
              <a href="{{ url('/') }}"><img class="" width="150px" src="{{ asset('web/img/logo-white.jpg') }}" alt=""></a>
            </div>
          </div>
          <div class="col-md-6">
            <div class="footer-text mt-2">
              <a href="{{ url('term-of-condition') }}">Our <strong>Terms and Condition</strong></a>
            </div>
          </div>
        </div>
      </div>
    </div>

    {{--<div class="footer_newsletter">
      <div class="container-fluid">
        <div class="row">
          <div class="col-xl-4 col-lg-4 col-md-6 col-sm-12 m-auto">
            <p class="text-center mb-4">
              <img src="{{asset('images/profile_icon/email.png')}}" width="50" alt="">
              <!--  <i class="fa fa-envelope-open-text text-warning" style="font-size:47px;"></i> -->
            </p>
            <h2 class="mb-4 w-100 text-center" style="text-transform: uppercase;">Subscribe to our
              <strong>Newsletter</strong></h2>

            <div class="input-group mb-4">
              <input type="text" class="form-control" name="subscribe_email" id="subscribe_email"
                     placeholder="Your Email">
              <div class="input-group-append">
                <span class="input-group-text newsletter-appended-btn" onclick="subscribe()" id="subscribe_btn">Subscribe</span>
              </div>
            </div>

          </div>
        </div>
      </div>
    </div>--}}

   {{-- <div class="container-fluid pb-3" style=" background:white">
      <div class="row">
        <div class="col-xl-4 col-lg-4 col-md-6 col-sm-12 m-auto">
          <div class="card card-body footer-form-fields">
            <input type="hidden" value="{{csrf_token()}}">
            <h2 style="text-transform:uppercase">Contact <strong> Us:</strong></h2>
            <input type="text" class="form-control" name="contact_name" id="contact_name" placeholder="Your name">
            <input type="emali" class="form-control" name="contact_email" id="contact_email" placeholder="Your email">
            <textarea cols="30" rows="5" class="form-control" name="contact_message" id="contact_message"
                      placeholder="Message"></textarea>
            <button onclick="sendMail()" class="btn btn-lg btn-warning" style="cursor:pointer;" id="contact_btn">SEND
              THIS
            </button>
          </div>
        </div>
      </div>
    </div>
    <div class="container-fluid pb-3" style="background:#f28f3a; height: 200px; margin-top:-200px;"></div>
    <div class="footer_menu">

      <div class="container-fluid">

        <div class="container foorter-menu-headline d-none d-sm-block">
          <div class="row">
            <div class="col-xl-3 col-lg-3 col-md-6 col-sm-12 ">
              <h2 class="text-white"><strong>About us:</strong></h2>
            </div>
            <div class="col-xl-3 col-lg-3 col-md-6 col-sm-12">
              <h2 class="text-white"><strong>Students:</strong></h2>
            </div>
            <div class="col-xl-3 col-lg-3 col-md-6 col-sm-12">
              <h2 class="text-white"><strong>Academic:</strong></h2>
            </div>
            <div class="col-xl-3 col-lg-3 col-md-6 col-sm-12">
              <h2 class="text-white"><strong>Social:</strong></h2>
            </div>
          </div>
        </div>
        <div class="horizontal-line d-none d-sm-block"></div>
        <div class="container">
          <div class="row">


            <div class="col-xl-3 col-lg-3 col-md-6 col-sm-12 col-xs-12">
              <div class="footer_menu_inner">
                <div class="d-block d-sm-none">
                  <h2 class="text-white"><strong>About us:</strong></h2>
                  <div class="horizontal-line"></div>
                </div>
                <ul>
                  <li><a href="{{ env('URL_SERVICES') }}">Services</a></li>
                  <li><a href="{{ env('URL_JOBS') }}">Jobs</a></li>
                  <li><a href="{{ env('URL_BLOG') }}">Blog</a></li>
                </ul>
              </div>
            </div>
            <div class="col-xl-3 col-lg-3 col-md-6 col-sm-12 col-xs-12">
              <div class="footer_menu_inner">
                <div class="d-block d-sm-none">
                  <h2 class="text-white"><strong>Students:</strong></h2>
                  <div class="horizontal-line"></div>
                </div>
                <ul>
                  <li><a href="{{ env('URL_ABOUT') }}">About</a></li>
                  <li><a href="{{ env('URL_NEWS_EVENT') }}">News and Events</a></li>
                  <li><a href="{{ env('URL_SUPPORT') }}">Support</a></li>
                  <li><a href="{{ env('URL_FAQ') }}">FAQs</a></li>
                </ul>
              </div>
            </div>
            <div class="col-xl-3 col-lg-3 col-md-6 col-sm-12 col-xs-12">
              <div class="footer_menu_inner">
                <div class="d-block d-sm-none">
                  <h2 class="text-white"><strong>Academic:</strong></h2>
                  <div class="horizontal-line"></div>
                </div>
                <ul>
                  <li><a href="{{ url('privacy-policy') }}">Privacy Policy</a></li>
                  <li><a href="{{ env('URL_HOW_WORK') }}">How it Works</a></li>
                </ul>
              </div>
            </div>
            <div class="col-xl-3 col-lg-3 col-md-6 col-sm-12 col-xs-12">
              <div class="footer_menu_inner">
                <div class="d-block d-sm-none">
                  <h2 class="text-white"><strong>Social:</strong></h2>
                  <div class="horizontal-line"></div>
                </div>
                <ul class="d-flex flex-wrap footer-social-icons">
                  <li><a href="{{ env('SOCIAL_FACEBOOK') }}"><i class="fab fa-facebook-f"></i></a></li>
                  <li><a href="{{ env('SOCIAL_PINTEREST') }}"><i class="fab fa-pinterest-p"></i></a></li>
                  <li><a href="{{ env('SOCIAL_INSTAGRAM') }}"><i class="fab fa-instagram"></i></a></li>
                  <li><a href="{{ env('SOCIAL_LINKEDIN') }}"><i class="fab fa-linkedin-in"></i></a></li>
                  <li><a href="{{ env('SOCIAL_YOUTUBE') }}"><i class="fab fa-youtube"></i></a></li>
                  <li><a href="{{ env('SOCIAL_TWITTER') }}"><i class="fab fa-twitter"></i></a></li>
                </ul>
              </div>
            </div>


          </div>
        </div>

      </div>

      <div class="footer_copyright">
        <div class="container">
          <div class="row">
            <div class="col-md-6">
              <div class="copy_left text-white">
                <strong>WooStudy</strong> © {{ date('Y') }}. All rights reserved.
              </div>
            </div>
            <div class="col-md-6">
              <div class="copy_right ">
                <a href="{{ url('term-of-condition') }}" class="text-white"> our <strong>Terms & Conditions</strong>
                </a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>--}}

  </footer>
</div>

<script src="{{ asset('js/app.js') }}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/owl.carousel.min.js"></script>
<link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.css"/>
<script type="text/javascript" src="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js"></script>
<script src="https://maps.googleapis.com/maps/api/js?key={{ env('GOOGLE_API_KEY') }}&libraries=places"></script>
@stack('js_stack')
<script>
  function sendMail() {
    var name = $('#contact_name').val();
    if (name == '') {
      toastr.error('Name is required!');
      return false;
    }

    var email = $('#contact_email').val();
    if (email == '') {
      toastr.error('Email is required!');
      return false;
    }

    var message = $('#contact_message').val();
    if (message == '') {
      toastr.error('Message is required!');
      return false;
    }
    $('#contact_btn').html('sending...');

    $.ajax({
      type: 'POST',
      url: "{{route('contact-send')}}",
      data: {'name': name, 'email': email, 'message': message},
      dataType: "json",
      headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      },
      success: function (response) {
        if (response.success) {
          toastr.success(response.message);
          $('#contact_name').val('');
          $('#contact_email').val('');
          $('#contact_message').val('');
          $('#contact_btn').html('SEND');
        } else {
          toastr.error(response.message);
        }
      },
      error: function (xhr, ajaxOptions, thrownError) {
        $('#contact_btn').html('SEND');
        toastr.error(xhr.status + ' : ' + thrownError);
      }
    });
  }

  function subscribe() {
    var email = $('#subscribe_email').val();
    if (email == '') {
      toastr.error('Subscribe Email is required!');
      return false;
    }

    $('#subscribe_btn').html('sending...');

    $.ajax({
      type: 'POST',
      url: "{{route('subscribe-email')}}",
      data: {'email': email},
      dataType: "json",
      headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      },
      success: function (response) {
        if (response.success) {
          toastr.success(response.message);
          $('#subscribe_email').val('');
          $('#subscribe_btn').html('Subscribe');
        } else {
          toastr.error(response.message);
        }
      },
      error: function (xhr, ajaxOptions, thrownError) {
        $('#subscribe_btn').html('Subscribe');
        toastr.error(xhr.status + ' : ' + thrownError);
      }
    });
  }

  //For social popup
  var popup;

  function ShowPopup(url) {
    popup = window.open(url, "Popup", "toolbar=no,scrollbars=no,location=no,statusbar=no,menubar=no,resizable=0,width=400,height=400,left = 490,top = 262");
    popup.focus();
  }

  function linkReload(authSlug) {
    if (authSlug == 1) {
      setTimeout(function () {
        window.location.href = "{{ url('home') }}";
      }, 500);
    } else {
      setTimeout(function () {
        window.location.href = "{{ url('set-role') }}";
      }, 500);
    }
  }
</script>

<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-65559240-4"></script>
<script>
  window.dataLayer = window.dataLayer || [];

  function gtag() {
    dataLayer.push(arguments);
  }

  gtag('js', new Date());

  gtag('config', 'UA-65559240-4');
</script>
</body>
</html>


