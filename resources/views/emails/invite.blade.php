{{ $user->name }} invite you to join {{ config('app.name', 'Laravel') }} for find Institute, Tutor and Student.<br>
<a href="{{ env('APP_URL') }}/public/profile/{{ $user->user_name }}">Click here</a> to connect with {{ $user->name }}<br>
or you can visit our website <a href="{{ env('APP_URL') }}">{{ env('APP_URL') }}</a>.<br><br>

@if($mailMessage!='')
  {{ $mailMessage }}<br><br>
@endif

Thank you<br>
{{ config('app.name', 'Laravel') }} Team
