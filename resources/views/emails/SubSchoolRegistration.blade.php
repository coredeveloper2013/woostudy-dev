{{ $user->name }} want to connect with your school in {{ config('app.name', 'Laravel') }}.<br>
<a href="{{ env('APP_URL') }}/public/profile">Click here</a> to approve the account check your login panel at {{ config('app.name', 'Laravel') }}.<br>
<br><br>

<br><br>

Thank you<br>
{{ config('app.name', 'Laravel') }} Team
