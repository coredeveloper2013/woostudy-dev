function testWhite(x) {
  var white = new RegExp(/^\s$/);
  return white.test(x.charAt(0));
}

export default function wordWrap(input, limit = 100) {
  let newLineStr = "<br/>"; let done = false; let res = '';
  while (input.length > limit) {
    let found = false;
    // Inserts new line at first whitespace of the line
    for (let i = limit - 1; i >= 0; i--) {
      if (testWhite(input.charAt(i))) {
        res = res + [input.slice(0, i), newLineStr].join('');
        input = input.slice(i + 1);
        found = true;
        break;
      }
    }
    // Inserts new line at limit position, the word is too long to wrap
    if (!found) {
      res += [input.slice(0, limit), newLineStr].join('');
      input = input.slice(limit);
    }

  }

  return res + input;
};
