import api from "../../config/api";

const state = {
    friends: []
};

const getters = {
    friends: state => state.friends
};

const mutations = {
    friendLists(state, payleoad) {
        state.friends = payleoad
    }
};

const actions = {
    async friendLists({commit}) {
        commit('friendLists',
            await axios.get(api.friendLists).then(response => {
                return response.data.data;
            }))
    }
};

export default {
    state,
    getters,
    actions,
    mutations
}