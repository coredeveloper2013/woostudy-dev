import api from "../../config/api";

const state = {
    ieltsExamLevels: {}
};

const getters = {
    ieltsExamLevels(state) {
        return state.ieltsExamLevels;
    }
};

const mutations = {
    setIeltsExamLevelMutation(state, payleoad) {
        return state.ieltsExamLevels = payleoad;
    },
    addNewIeltsExamLevelMutation(state, payleoad) {
        Vue.set(state.ieltsExamLevels, state.ieltsExamLevels.length, payleoad)
    }
};

const actions = {
    async setIeltsExamLevelAction({commit}) {
        commit('setIeltsExamLevelMutation',
            await axios.get(api.ieltsExamLevel).then(response => {
                return response.data;
            }))
    },
    async addNewIeltsExamLevelAction({commit}, payload) {
        commit('addNewIeltsExamLevelMutation',
            await axios.post(api.ieltsExamLevel, payload).then(response => {
                return response.data;
            }))
    }
};

export default {
    state,
    getters,
    actions,
    mutations
}