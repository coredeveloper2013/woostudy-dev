import api from "../../config/api";

const state = {
    educationLevels: {}
};

const getters = {
    educationLevels(state) {
        return state.educationLevels;
    }
};

const mutations = {
    setEducationLevelMutation(state, payleoad) {
        return state.educationLevels = payleoad;
    },
    addNewEducationLevelMutation(state, payleoad) {
        Vue.set(state.educationLevels, state.educationLevels.length, payleoad)
    }
};

const actions = {
    async setEducationLevelAction({commit}) {
        commit('setEducationLevelMutation',
            await axios.get(api.educationLevel).then(response => {
                return response.data;
            }))
    },
    async addNewEducationLevelAction({commit}, payload) {
        commit('addNewEducationLevelMutation',
            await axios.post(api.educationLevel, payload).then(response => {
                return response.data;
            }))
    }
};

export default {
    state,
    getters,
    actions,
    mutations
}