-- phpMyAdmin SQL Dump
-- version 4.5.4.1deb2ubuntu2.1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: May 14, 2019 at 04:54 PM
-- Server version: 5.7.24-0ubuntu0.16.04.1
-- PHP Version: 7.2.13-1+ubuntu16.04.1+deb.sury.org+1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `woostudywebapp`
--

-- --------------------------------------------------------

--
-- Table structure for table `addresses`
--

DROP TABLE IF EXISTS `addresses`;
CREATE TABLE `addresses` (
  `id` int(10) UNSIGNED NOT NULL,
  `country_id` int(10) UNSIGNED NOT NULL,
  `state_id` int(10) UNSIGNED DEFAULT NULL,
  `city` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `lat` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `lng` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `addressable_id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `addressable_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `addresses`
--

INSERT INTO `addresses` (`id`, `country_id`, `state_id`, `city`, `address`, `lat`, `lng`, `addressable_id`, `addressable_type`, `created_at`, `updated_at`) VALUES
(1, 240, NULL, 'Irvine', 'Elk Grove, Irvine, CA 92618, USA', '33.6686763', '-117.7811643', '1', 'App\\User', '2019-05-14 12:16:40', '2019-05-14 12:16:40'),
(2, 240, NULL, 'Irvine', '4321 Walnut Ave.  Irvine, CA 92604-2239', NULL, NULL, '2', 'App\\User', '2019-05-14 14:31:40', '2019-05-14 14:31:40');

-- --------------------------------------------------------

--
-- Table structure for table `chattings`
--

DROP TABLE IF EXISTS `chattings`;
CREATE TABLE `chattings` (
  `id` int(10) UNSIGNED NOT NULL,
  `from` int(10) UNSIGNED NOT NULL,
  `to` int(10) UNSIGNED NOT NULL,
  `message` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `read_at` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `chatting_blocks`
--

DROP TABLE IF EXISTS `chatting_blocks`;
CREATE TABLE `chatting_blocks` (
  `id` int(10) UNSIGNED NOT NULL,
  `from` int(10) UNSIGNED NOT NULL,
  `to` int(10) UNSIGNED NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0 => No, 1 => Yes',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `comments`
--

DROP TABLE IF EXISTS `comments`;
CREATE TABLE `comments` (
  `id` int(10) UNSIGNED NOT NULL,
  `post_id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1',
  `body` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `countries`
--

DROP TABLE IF EXISTS `countries`;
CREATE TABLE `countries` (
  `id` int(10) UNSIGNED NOT NULL,
  `region_id` int(10) UNSIGNED NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `code` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `flag` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `countries`
--

INSERT INTO `countries` (`id`, `region_id`, `title`, `code`, `slug`, `flag`, `created_at`, `updated_at`) VALUES
(1, 3, 'Afghanistan', 'AFG', 'Afghanistan', 'https://restcountries.eu/data/afg.svg', NULL, NULL),
(2, 4, 'Åland Islands', 'ALA', 'Åland Islands', 'https://restcountries.eu/data/ala.svg', NULL, NULL),
(3, 4, 'Albania', 'ALB', 'Albania', 'https://restcountries.eu/data/alb.svg', NULL, NULL),
(4, 1, 'Algeria', 'DZA', 'Algeria', 'https://restcountries.eu/data/dza.svg', NULL, NULL),
(5, 5, 'American Samoa', 'ASM', 'American Samoa', 'https://restcountries.eu/data/asm.svg', NULL, NULL),
(6, 4, 'Andorra', 'AND', 'Andorra', 'https://restcountries.eu/data/and.svg', NULL, NULL),
(7, 1, 'Angola', 'AGO', 'Angola', 'https://restcountries.eu/data/ago.svg', NULL, NULL),
(8, 2, 'Anguilla', 'AIA', 'Anguilla', 'https://restcountries.eu/data/aia.svg', NULL, NULL),
(9, 5, 'Antarctica', 'ATA', 'Antarctica', 'https://restcountries.eu/data/ata.svg', NULL, NULL),
(10, 2, 'Antigua and Barbuda', 'ATG', 'Antigua and Barbuda', 'https://restcountries.eu/data/atg.svg', NULL, NULL),
(11, 2, 'Argentina', 'ARG', 'Argentina', 'https://restcountries.eu/data/arg.svg', NULL, NULL),
(12, 3, 'Armenia', 'ARM', 'Armenia', 'https://restcountries.eu/data/arm.svg', NULL, NULL),
(13, 2, 'Aruba', 'ABW', 'Aruba', 'https://restcountries.eu/data/abw.svg', NULL, NULL),
(14, 5, 'Australia', 'AUS', 'Australia', 'https://restcountries.eu/data/aus.svg', NULL, NULL),
(15, 4, 'Austria', 'AUT', 'Austria', 'https://restcountries.eu/data/aut.svg', NULL, NULL),
(16, 3, 'Azerbaijan', 'AZE', 'Azerbaijan', 'https://restcountries.eu/data/aze.svg', NULL, NULL),
(17, 2, 'Bahamas', 'BHS', 'Bahamas', 'https://restcountries.eu/data/bhs.svg', NULL, NULL),
(18, 3, 'Bahrain', 'BHR', 'Bahrain', 'https://restcountries.eu/data/bhr.svg', NULL, NULL),
(19, 3, 'Bangladesh', 'BGD', 'Bangladesh', 'https://restcountries.eu/data/bgd.svg', NULL, NULL),
(20, 2, 'Barbados', 'BRB', 'Barbados', 'https://restcountries.eu/data/brb.svg', NULL, NULL),
(21, 4, 'Belarus', 'BLR', 'Belarus', 'https://restcountries.eu/data/blr.svg', NULL, NULL),
(22, 4, 'Belgium', 'BEL', 'Belgium', 'https://restcountries.eu/data/bel.svg', NULL, NULL),
(23, 2, 'Belize', 'BLZ', 'Belize', 'https://restcountries.eu/data/blz.svg', NULL, NULL),
(24, 1, 'Benin', 'BEN', 'Benin', 'https://restcountries.eu/data/ben.svg', NULL, NULL),
(25, 2, 'Bermuda', 'BMU', 'Bermuda', 'https://restcountries.eu/data/bmu.svg', NULL, NULL),
(26, 3, 'Bhutan', 'BTN', 'Bhutan', 'https://restcountries.eu/data/btn.svg', NULL, NULL),
(27, 2, 'Bolivia (Plurinational State of)', 'BOL', 'Bolivia (Plurinational State of)', 'https://restcountries.eu/data/bol.svg', NULL, NULL),
(28, 2, 'Bonaire, Sint Eustatius and Saba', 'BES', 'Bonaire, Sint Eustatius and Saba', 'https://restcountries.eu/data/bes.svg', NULL, NULL),
(29, 4, 'Bosnia and Herzegovina', 'BIH', 'Bosnia and Herzegovina', 'https://restcountries.eu/data/bih.svg', NULL, NULL),
(30, 1, 'Botswana', 'BWA', 'Botswana', 'https://restcountries.eu/data/bwa.svg', NULL, NULL),
(31, 2, 'Bouvet Island', 'BVT', 'Bouvet Island', 'https://restcountries.eu/data/bvt.svg', NULL, NULL),
(32, 2, 'Brazil', 'BRA', 'Brazil', 'https://restcountries.eu/data/bra.svg', NULL, NULL),
(33, 1, 'British Indian Ocean Territory', 'IOT', 'British Indian Ocean Territory', 'https://restcountries.eu/data/iot.svg', NULL, NULL),
(34, 2, 'United States Minor Outlying Islands', 'UMI', 'United States Minor Outlying Islands', 'https://restcountries.eu/data/umi.svg', NULL, NULL),
(35, 2, 'Virgin Islands (British)', 'VGB', 'Virgin Islands (British)', 'https://restcountries.eu/data/vgb.svg', NULL, NULL),
(36, 2, 'Virgin Islands (U.S.)', 'VIR', 'Virgin Islands (U.S.)', 'https://restcountries.eu/data/vir.svg', NULL, NULL),
(37, 3, 'Brunei Darussalam', 'BRN', 'Brunei Darussalam', 'https://restcountries.eu/data/brn.svg', NULL, NULL),
(38, 4, 'Bulgaria', 'BGR', 'Bulgaria', 'https://restcountries.eu/data/bgr.svg', NULL, NULL),
(39, 1, 'Burkina Faso', 'BFA', 'Burkina Faso', 'https://restcountries.eu/data/bfa.svg', NULL, NULL),
(40, 1, 'Burundi', 'BDI', 'Burundi', 'https://restcountries.eu/data/bdi.svg', NULL, NULL),
(41, 3, 'Cambodia', 'KHM', 'Cambodia', 'https://restcountries.eu/data/khm.svg', NULL, NULL),
(42, 1, 'Cameroon', 'CMR', 'Cameroon', 'https://restcountries.eu/data/cmr.svg', NULL, NULL),
(43, 2, 'Canada', 'CAN', 'Canada', 'https://restcountries.eu/data/can.svg', NULL, NULL),
(44, 1, 'Cabo Verde', 'CPV', 'Cabo Verde', 'https://restcountries.eu/data/cpv.svg', NULL, NULL),
(45, 2, 'Cayman Islands', 'CYM', 'Cayman Islands', 'https://restcountries.eu/data/cym.svg', NULL, NULL),
(46, 1, 'Central African Republic', 'CAF', 'Central African Republic', 'https://restcountries.eu/data/caf.svg', NULL, NULL),
(47, 1, 'Chad', 'TCD', 'Chad', 'https://restcountries.eu/data/tcd.svg', NULL, NULL),
(48, 2, 'Chile', 'CHL', 'Chile', 'https://restcountries.eu/data/chl.svg', NULL, NULL),
(49, 3, 'China', 'CHN', 'China', 'https://restcountries.eu/data/chn.svg', NULL, NULL),
(50, 5, 'Christmas Island', 'CXR', 'Christmas Island', 'https://restcountries.eu/data/cxr.svg', NULL, NULL),
(51, 5, 'Cocos (Keeling) Islands', 'CCK', 'Cocos (Keeling) Islands', 'https://restcountries.eu/data/cck.svg', NULL, NULL),
(52, 2, 'Colombia', 'COL', 'Colombia', 'https://restcountries.eu/data/col.svg', NULL, NULL),
(53, 1, 'Comoros', 'COM', 'Comoros', 'https://restcountries.eu/data/com.svg', NULL, NULL),
(54, 1, 'Congo', 'COG', 'Congo', 'https://restcountries.eu/data/cog.svg', NULL, NULL),
(55, 1, 'Congo (Democratic Republic of the)', 'COD', 'Congo (Democratic Republic of the)', 'https://restcountries.eu/data/cod.svg', NULL, NULL),
(56, 5, 'Cook Islands', 'COK', 'Cook Islands', 'https://restcountries.eu/data/cok.svg', NULL, NULL),
(57, 2, 'Costa Rica', 'CRI', 'Costa Rica', 'https://restcountries.eu/data/cri.svg', NULL, NULL),
(58, 4, 'Croatia', 'HRV', 'Croatia', 'https://restcountries.eu/data/hrv.svg', NULL, NULL),
(59, 2, 'Cuba', 'CUB', 'Cuba', 'https://restcountries.eu/data/cub.svg', NULL, NULL),
(60, 2, 'Curaçao', 'CUW', 'Curaçao', 'https://restcountries.eu/data/cuw.svg', NULL, NULL),
(61, 4, 'Cyprus', 'CYP', 'Cyprus', 'https://restcountries.eu/data/cyp.svg', NULL, NULL),
(62, 4, 'Czech Republic', 'CZE', 'Czech Republic', 'https://restcountries.eu/data/cze.svg', NULL, NULL),
(63, 4, 'Denmark', 'DNK', 'Denmark', 'https://restcountries.eu/data/dnk.svg', NULL, NULL),
(64, 1, 'Djibouti', 'DJI', 'Djibouti', 'https://restcountries.eu/data/dji.svg', NULL, NULL),
(65, 2, 'Dominica', 'DMA', 'Dominica', 'https://restcountries.eu/data/dma.svg', NULL, NULL),
(66, 2, 'Dominican Republic', 'DOM', 'Dominican Republic', 'https://restcountries.eu/data/dom.svg', NULL, NULL),
(67, 2, 'Ecuador', 'ECU', 'Ecuador', 'https://restcountries.eu/data/ecu.svg', NULL, NULL),
(68, 1, 'Egypt', 'EGY', 'Egypt', 'https://restcountries.eu/data/egy.svg', NULL, NULL),
(69, 2, 'El Salvador', 'SLV', 'El Salvador', 'https://restcountries.eu/data/slv.svg', NULL, NULL),
(70, 1, 'Equatorial Guinea', 'GNQ', 'Equatorial Guinea', 'https://restcountries.eu/data/gnq.svg', NULL, NULL),
(71, 1, 'Eritrea', 'ERI', 'Eritrea', 'https://restcountries.eu/data/eri.svg', NULL, NULL),
(72, 4, 'Estonia', 'EST', 'Estonia', 'https://restcountries.eu/data/est.svg', NULL, NULL),
(73, 1, 'Ethiopia', 'ETH', 'Ethiopia', 'https://restcountries.eu/data/eth.svg', NULL, NULL),
(74, 2, 'Falkland Islands (Malvinas)', 'FLK', 'Falkland Islands (Malvinas)', 'https://restcountries.eu/data/flk.svg', NULL, NULL),
(75, 4, 'Faroe Islands', 'FRO', 'Faroe Islands', 'https://restcountries.eu/data/fro.svg', NULL, NULL),
(76, 5, 'Fiji', 'FJI', 'Fiji', 'https://restcountries.eu/data/fji.svg', NULL, NULL),
(77, 4, 'Finland', 'FIN', 'Finland', 'https://restcountries.eu/data/fin.svg', NULL, NULL),
(78, 4, 'France', 'FRA', 'France', 'https://restcountries.eu/data/fra.svg', NULL, NULL),
(79, 2, 'French Guiana', 'GUF', 'French Guiana', 'https://restcountries.eu/data/guf.svg', NULL, NULL),
(80, 5, 'French Polynesia', 'PYF', 'French Polynesia', 'https://restcountries.eu/data/pyf.svg', NULL, NULL),
(81, 1, 'French Southern Territories', 'ATF', 'French Southern Territories', 'https://restcountries.eu/data/atf.svg', NULL, NULL),
(82, 1, 'Gabon', 'GAB', 'Gabon', 'https://restcountries.eu/data/gab.svg', NULL, NULL),
(83, 1, 'Gambia', 'GMB', 'Gambia', 'https://restcountries.eu/data/gmb.svg', NULL, NULL),
(84, 3, 'Georgia', 'GEO', 'Georgia', 'https://restcountries.eu/data/geo.svg', NULL, NULL),
(85, 4, 'Germany', 'DEU', 'Germany', 'https://restcountries.eu/data/deu.svg', NULL, NULL),
(86, 1, 'Ghana', 'GHA', 'Ghana', 'https://restcountries.eu/data/gha.svg', NULL, NULL),
(87, 4, 'Gibraltar', 'GIB', 'Gibraltar', 'https://restcountries.eu/data/gib.svg', NULL, NULL),
(88, 4, 'Greece', 'GRC', 'Greece', 'https://restcountries.eu/data/grc.svg', NULL, NULL),
(89, 2, 'Greenland', 'GRL', 'Greenland', 'https://restcountries.eu/data/grl.svg', NULL, NULL),
(90, 2, 'Grenada', 'GRD', 'Grenada', 'https://restcountries.eu/data/grd.svg', NULL, NULL),
(91, 2, 'Guadeloupe', 'GLP', 'Guadeloupe', 'https://restcountries.eu/data/glp.svg', NULL, NULL),
(92, 5, 'Guam', 'GUM', 'Guam', 'https://restcountries.eu/data/gum.svg', NULL, NULL),
(93, 2, 'Guatemala', 'GTM', 'Guatemala', 'https://restcountries.eu/data/gtm.svg', NULL, NULL),
(94, 4, 'Guernsey', 'GGY', 'Guernsey', 'https://restcountries.eu/data/ggy.svg', NULL, NULL),
(95, 1, 'Guinea', 'GIN', 'Guinea', 'https://restcountries.eu/data/gin.svg', NULL, NULL),
(96, 1, 'Guinea-Bissau', 'GNB', 'Guinea-Bissau', 'https://restcountries.eu/data/gnb.svg', NULL, NULL),
(97, 2, 'Guyana', 'GUY', 'Guyana', 'https://restcountries.eu/data/guy.svg', NULL, NULL),
(98, 2, 'Haiti', 'HTI', 'Haiti', 'https://restcountries.eu/data/hti.svg', NULL, NULL),
(99, 4, 'Heard Island and McDonald Islands', 'HMD', 'Heard Island and McDonald Islands', 'https://restcountries.eu/data/hmd.svg', NULL, NULL),
(100, 4, 'Holy See', 'VAT', 'Holy See', 'https://restcountries.eu/data/vat.svg', NULL, NULL),
(101, 2, 'Honduras', 'HND', 'Honduras', 'https://restcountries.eu/data/hnd.svg', NULL, NULL),
(102, 3, 'Hong Kong', 'HKG', 'Hong Kong', 'https://restcountries.eu/data/hkg.svg', NULL, NULL),
(103, 4, 'Hungary', 'HUN', 'Hungary', 'https://restcountries.eu/data/hun.svg', NULL, NULL),
(104, 4, 'Iceland', 'ISL', 'Iceland', 'https://restcountries.eu/data/isl.svg', NULL, NULL),
(105, 3, 'India', 'IND', 'India', 'https://restcountries.eu/data/ind.svg', NULL, NULL),
(106, 3, 'Indonesia', 'IDN', 'Indonesia', 'https://restcountries.eu/data/idn.svg', NULL, NULL),
(107, 1, 'Côte d\'Ivoire', 'CIV', 'Côte d\'Ivoire', 'https://restcountries.eu/data/civ.svg', NULL, NULL),
(108, 3, 'Iran (Islamic Republic of)', 'IRN', 'Iran (Islamic Republic of)', 'https://restcountries.eu/data/irn.svg', NULL, NULL),
(109, 3, 'Iraq', 'IRQ', 'Iraq', 'https://restcountries.eu/data/irq.svg', NULL, NULL),
(110, 4, 'Ireland', 'IRL', 'Ireland', 'https://restcountries.eu/data/irl.svg', NULL, NULL),
(111, 4, 'Isle of Man', 'IMN', 'Isle of Man', 'https://restcountries.eu/data/imn.svg', NULL, NULL),
(112, 3, 'Israel', 'ISR', 'Israel', 'https://restcountries.eu/data/isr.svg', NULL, NULL),
(113, 4, 'Italy', 'ITA', 'Italy', 'https://restcountries.eu/data/ita.svg', NULL, NULL),
(114, 2, 'Jamaica', 'JAM', 'Jamaica', 'https://restcountries.eu/data/jam.svg', NULL, NULL),
(115, 3, 'Japan', 'JPN', 'Japan', 'https://restcountries.eu/data/jpn.svg', NULL, NULL),
(116, 4, 'Jersey', 'JEY', 'Jersey', 'https://restcountries.eu/data/jey.svg', NULL, NULL),
(117, 3, 'Jordan', 'JOR', 'Jordan', 'https://restcountries.eu/data/jor.svg', NULL, NULL),
(118, 3, 'Kazakhstan', 'KAZ', 'Kazakhstan', 'https://restcountries.eu/data/kaz.svg', NULL, NULL),
(119, 1, 'Kenya', 'KEN', 'Kenya', 'https://restcountries.eu/data/ken.svg', NULL, NULL),
(120, 5, 'Kiribati', 'KIR', 'Kiribati', 'https://restcountries.eu/data/kir.svg', NULL, NULL),
(121, 3, 'Kuwait', 'KWT', 'Kuwait', 'https://restcountries.eu/data/kwt.svg', NULL, NULL),
(122, 3, 'Kyrgyzstan', 'KGZ', 'Kyrgyzstan', 'https://restcountries.eu/data/kgz.svg', NULL, NULL),
(123, 3, 'Lao People\'s Democratic Republic', 'LAO', 'Lao People\'s Democratic Republic', 'https://restcountries.eu/data/lao.svg', NULL, NULL),
(124, 4, 'Latvia', 'LVA', 'Latvia', 'https://restcountries.eu/data/lva.svg', NULL, NULL),
(125, 3, 'Lebanon', 'LBN', 'Lebanon', 'https://restcountries.eu/data/lbn.svg', NULL, NULL),
(126, 1, 'Lesotho', 'LSO', 'Lesotho', 'https://restcountries.eu/data/lso.svg', NULL, NULL),
(127, 1, 'Liberia', 'LBR', 'Liberia', 'https://restcountries.eu/data/lbr.svg', NULL, NULL),
(128, 1, 'Libya', 'LBY', 'Libya', 'https://restcountries.eu/data/lby.svg', NULL, NULL),
(129, 4, 'Liechtenstein', 'LIE', 'Liechtenstein', 'https://restcountries.eu/data/lie.svg', NULL, NULL),
(130, 4, 'Lithuania', 'LTU', 'Lithuania', 'https://restcountries.eu/data/ltu.svg', NULL, NULL),
(131, 4, 'Luxembourg', 'LUX', 'Luxembourg', 'https://restcountries.eu/data/lux.svg', NULL, NULL),
(132, 3, 'Macao', 'MAC', 'Macao', 'https://restcountries.eu/data/mac.svg', NULL, NULL),
(133, 4, 'Macedonia (the former Yugoslav Republic of)', 'MKD', 'Macedonia (the former Yugoslav Republic of)', 'https://restcountries.eu/data/mkd.svg', NULL, NULL),
(134, 1, 'Madagascar', 'MDG', 'Madagascar', 'https://restcountries.eu/data/mdg.svg', NULL, NULL),
(135, 1, 'Malawi', 'MWI', 'Malawi', 'https://restcountries.eu/data/mwi.svg', NULL, NULL),
(136, 3, 'Malaysia', 'MYS', 'Malaysia', 'https://restcountries.eu/data/mys.svg', NULL, NULL),
(137, 3, 'Maldives', 'MDV', 'Maldives', 'https://restcountries.eu/data/mdv.svg', NULL, NULL),
(138, 1, 'Mali', 'MLI', 'Mali', 'https://restcountries.eu/data/mli.svg', NULL, NULL),
(139, 4, 'Malta', 'MLT', 'Malta', 'https://restcountries.eu/data/mlt.svg', NULL, NULL),
(140, 5, 'Marshall Islands', 'MHL', 'Marshall Islands', 'https://restcountries.eu/data/mhl.svg', NULL, NULL),
(141, 2, 'Martinique', 'MTQ', 'Martinique', 'https://restcountries.eu/data/mtq.svg', NULL, NULL),
(142, 1, 'Mauritania', 'MRT', 'Mauritania', 'https://restcountries.eu/data/mrt.svg', NULL, NULL),
(143, 1, 'Mauritius', 'MUS', 'Mauritius', 'https://restcountries.eu/data/mus.svg', NULL, NULL),
(144, 1, 'Mayotte', 'MYT', 'Mayotte', 'https://restcountries.eu/data/myt.svg', NULL, NULL),
(145, 2, 'Mexico', 'MEX', 'Mexico', 'https://restcountries.eu/data/mex.svg', NULL, NULL),
(146, 5, 'Micronesia (Federated States of)', 'FSM', 'Micronesia (Federated States of)', 'https://restcountries.eu/data/fsm.svg', NULL, NULL),
(147, 4, 'Moldova (Republic of)', 'MDA', 'Moldova (Republic of)', 'https://restcountries.eu/data/mda.svg', NULL, NULL),
(148, 4, 'Monaco', 'MCO', 'Monaco', 'https://restcountries.eu/data/mco.svg', NULL, NULL),
(149, 3, 'Mongolia', 'MNG', 'Mongolia', 'https://restcountries.eu/data/mng.svg', NULL, NULL),
(150, 4, 'Montenegro', 'MNE', 'Montenegro', 'https://restcountries.eu/data/mne.svg', NULL, NULL),
(151, 2, 'Montserrat', 'MSR', 'Montserrat', 'https://restcountries.eu/data/msr.svg', NULL, NULL),
(152, 1, 'Morocco', 'MAR', 'Morocco', 'https://restcountries.eu/data/mar.svg', NULL, NULL),
(153, 1, 'Mozambique', 'MOZ', 'Mozambique', 'https://restcountries.eu/data/moz.svg', NULL, NULL),
(154, 3, 'Myanmar', 'MMR', 'Myanmar', 'https://restcountries.eu/data/mmr.svg', NULL, NULL),
(155, 1, 'Namibia', 'NAM', 'Namibia', 'https://restcountries.eu/data/nam.svg', NULL, NULL),
(156, 5, 'Nauru', 'NRU', 'Nauru', 'https://restcountries.eu/data/nru.svg', NULL, NULL),
(157, 3, 'Nepal', 'NPL', 'Nepal', 'https://restcountries.eu/data/npl.svg', NULL, NULL),
(158, 4, 'Netherlands', 'NLD', 'Netherlands', 'https://restcountries.eu/data/nld.svg', NULL, NULL),
(159, 5, 'New Caledonia', 'NCL', 'New Caledonia', 'https://restcountries.eu/data/ncl.svg', NULL, NULL),
(160, 5, 'New Zealand', 'NZL', 'New Zealand', 'https://restcountries.eu/data/nzl.svg', NULL, NULL),
(161, 2, 'Nicaragua', 'NIC', 'Nicaragua', 'https://restcountries.eu/data/nic.svg', NULL, NULL),
(162, 1, 'Niger', 'NER', 'Niger', 'https://restcountries.eu/data/ner.svg', NULL, NULL),
(163, 1, 'Nigeria', 'NGA', 'Nigeria', 'https://restcountries.eu/data/nga.svg', NULL, NULL),
(164, 5, 'Niue', 'NIU', 'Niue', 'https://restcountries.eu/data/niu.svg', NULL, NULL),
(165, 5, 'Norfolk Island', 'NFK', 'Norfolk Island', 'https://restcountries.eu/data/nfk.svg', NULL, NULL),
(166, 3, 'Korea (Democratic People\'s Republic of)', 'PRK', 'Korea (Democratic People\'s Republic of)', 'https://restcountries.eu/data/prk.svg', NULL, NULL),
(167, 5, 'Northern Mariana Islands', 'MNP', 'Northern Mariana Islands', 'https://restcountries.eu/data/mnp.svg', NULL, NULL),
(168, 4, 'Norway', 'NOR', 'Norway', 'https://restcountries.eu/data/nor.svg', NULL, NULL),
(169, 3, 'Oman', 'OMN', 'Oman', 'https://restcountries.eu/data/omn.svg', NULL, NULL),
(170, 3, 'Pakistan', 'PAK', 'Pakistan', 'https://restcountries.eu/data/pak.svg', NULL, NULL),
(171, 5, 'Palau', 'PLW', 'Palau', 'https://restcountries.eu/data/plw.svg', NULL, NULL),
(172, 3, 'Palestine, State of', 'PSE', 'Palestine, State of', 'https://restcountries.eu/data/pse.svg', NULL, NULL),
(173, 2, 'Panama', 'PAN', 'Panama', 'https://restcountries.eu/data/pan.svg', NULL, NULL),
(174, 5, 'Papua New Guinea', 'PNG', 'Papua New Guinea', 'https://restcountries.eu/data/png.svg', NULL, NULL),
(175, 2, 'Paraguay', 'PRY', 'Paraguay', 'https://restcountries.eu/data/pry.svg', NULL, NULL),
(176, 2, 'Peru', 'PER', 'Peru', 'https://restcountries.eu/data/per.svg', NULL, NULL),
(177, 3, 'Philippines', 'PHL', 'Philippines', 'https://restcountries.eu/data/phl.svg', NULL, NULL),
(178, 5, 'Pitcairn', 'PCN', 'Pitcairn', 'https://restcountries.eu/data/pcn.svg', NULL, NULL),
(179, 4, 'Poland', 'POL', 'Poland', 'https://restcountries.eu/data/pol.svg', NULL, NULL),
(180, 4, 'Portugal', 'PRT', 'Portugal', 'https://restcountries.eu/data/prt.svg', NULL, NULL),
(181, 2, 'Puerto Rico', 'PRI', 'Puerto Rico', 'https://restcountries.eu/data/pri.svg', NULL, NULL),
(182, 3, 'Qatar', 'QAT', 'Qatar', 'https://restcountries.eu/data/qat.svg', NULL, NULL),
(183, 4, 'Republic of Kosovo', 'KOS', 'Republic of Kosovo', 'https://restcountries.eu/data/kos.svg', NULL, NULL),
(184, 1, 'Réunion', 'REU', 'Réunion', 'https://restcountries.eu/data/reu.svg', NULL, NULL),
(185, 4, 'Romania', 'ROU', 'Romania', 'https://restcountries.eu/data/rou.svg', NULL, NULL),
(186, 4, 'Russian Federation', 'RUS', 'Russian Federation', 'https://restcountries.eu/data/rus.svg', NULL, NULL),
(187, 1, 'Rwanda', 'RWA', 'Rwanda', 'https://restcountries.eu/data/rwa.svg', NULL, NULL),
(188, 2, 'Saint Barthélemy', 'BLM', 'Saint Barthélemy', 'https://restcountries.eu/data/blm.svg', NULL, NULL),
(189, 1, 'Saint Helena, Ascension and Tristan da Cunha', 'SHN', 'Saint Helena, Ascension and Tristan da Cunha', 'https://restcountries.eu/data/shn.svg', NULL, NULL),
(190, 2, 'Saint Kitts and Nevis', 'KNA', 'Saint Kitts and Nevis', 'https://restcountries.eu/data/kna.svg', NULL, NULL),
(191, 2, 'Saint Lucia', 'LCA', 'Saint Lucia', 'https://restcountries.eu/data/lca.svg', NULL, NULL),
(192, 2, 'Saint Martin (French part)', 'MAF', 'Saint Martin (French part)', 'https://restcountries.eu/data/maf.svg', NULL, NULL),
(193, 2, 'Saint Pierre and Miquelon', 'SPM', 'Saint Pierre and Miquelon', 'https://restcountries.eu/data/spm.svg', NULL, NULL),
(194, 2, 'Saint Vincent and the Grenadines', 'VCT', 'Saint Vincent and the Grenadines', 'https://restcountries.eu/data/vct.svg', NULL, NULL),
(195, 5, 'Samoa', 'WSM', 'Samoa', 'https://restcountries.eu/data/wsm.svg', NULL, NULL),
(196, 4, 'San Marino', 'SMR', 'San Marino', 'https://restcountries.eu/data/smr.svg', NULL, NULL),
(197, 1, 'Sao Tome and Principe', 'STP', 'Sao Tome and Principe', 'https://restcountries.eu/data/stp.svg', NULL, NULL),
(198, 3, 'Saudi Arabia', 'SAU', 'Saudi Arabia', 'https://restcountries.eu/data/sau.svg', NULL, NULL),
(199, 1, 'Senegal', 'SEN', 'Senegal', 'https://restcountries.eu/data/sen.svg', NULL, NULL),
(200, 4, 'Serbia', 'SRB', 'Serbia', 'https://restcountries.eu/data/srb.svg', NULL, NULL),
(201, 1, 'Seychelles', 'SYC', 'Seychelles', 'https://restcountries.eu/data/syc.svg', NULL, NULL),
(202, 1, 'Sierra Leone', 'SLE', 'Sierra Leone', 'https://restcountries.eu/data/sle.svg', NULL, NULL),
(203, 3, 'Singapore', 'SGP', 'Singapore', 'https://restcountries.eu/data/sgp.svg', NULL, NULL),
(204, 2, 'Sint Maarten (Dutch part)', 'SXM', 'Sint Maarten (Dutch part)', 'https://restcountries.eu/data/sxm.svg', NULL, NULL),
(205, 4, 'Slovakia', 'SVK', 'Slovakia', 'https://restcountries.eu/data/svk.svg', NULL, NULL),
(206, 4, 'Slovenia', 'SVN', 'Slovenia', 'https://restcountries.eu/data/svn.svg', NULL, NULL),
(207, 5, 'Solomon Islands', 'SLB', 'Solomon Islands', 'https://restcountries.eu/data/slb.svg', NULL, NULL),
(208, 1, 'Somalia', 'SOM', 'Somalia', 'https://restcountries.eu/data/som.svg', NULL, NULL),
(209, 1, 'South Africa', 'ZAF', 'South Africa', 'https://restcountries.eu/data/zaf.svg', NULL, NULL),
(210, 2, 'South Georgia and the South Sandwich Islands', 'SGS', 'South Georgia and the South Sandwich Islands', 'https://restcountries.eu/data/sgs.svg', NULL, NULL),
(211, 3, 'Korea (Republic of)', 'KOR', 'Korea (Republic of)', 'https://restcountries.eu/data/kor.svg', NULL, NULL),
(212, 1, 'South Sudan', 'SSD', 'South Sudan', 'https://restcountries.eu/data/ssd.svg', NULL, NULL),
(213, 4, 'Spain', 'ESP', 'Spain', 'https://restcountries.eu/data/esp.svg', NULL, NULL),
(214, 3, 'Sri Lanka', 'LKA', 'Sri Lanka', 'https://restcountries.eu/data/lka.svg', NULL, NULL),
(215, 1, 'Sudan', 'SDN', 'Sudan', 'https://restcountries.eu/data/sdn.svg', NULL, NULL),
(216, 2, 'Suriname', 'SUR', 'Suriname', 'https://restcountries.eu/data/sur.svg', NULL, NULL),
(217, 4, 'Svalbard and Jan Mayen', 'SJM', 'Svalbard and Jan Mayen', 'https://restcountries.eu/data/sjm.svg', NULL, NULL),
(218, 1, 'Swaziland', 'SWZ', 'Swaziland', 'https://restcountries.eu/data/swz.svg', NULL, NULL),
(219, 4, 'Sweden', 'SWE', 'Sweden', 'https://restcountries.eu/data/swe.svg', NULL, NULL),
(220, 4, 'Switzerland', 'CHE', 'Switzerland', 'https://restcountries.eu/data/che.svg', NULL, NULL),
(221, 1, 'Syrian Arab Republic', 'SYR', 'Syrian Arab Republic', 'https://restcountries.eu/data/syr.svg', NULL, NULL),
(222, 3, 'Taiwan', 'TWN', 'Taiwan', 'https://restcountries.eu/data/twn.svg', NULL, NULL),
(223, 3, 'Tajikistan', 'TJK', 'Tajikistan', 'https://restcountries.eu/data/tjk.svg', NULL, NULL),
(224, 1, 'Tanzania, United Republic of', 'TZA', 'Tanzania, United Republic of', 'https://restcountries.eu/data/tza.svg', NULL, NULL),
(225, 3, 'Thailand', 'THA', 'Thailand', 'https://restcountries.eu/data/tha.svg', NULL, NULL),
(226, 3, 'Timor-Leste', 'TLS', 'Timor-Leste', 'https://restcountries.eu/data/tls.svg', NULL, NULL),
(227, 1, 'Togo', 'TGO', 'Togo', 'https://restcountries.eu/data/tgo.svg', NULL, NULL),
(228, 5, 'Tokelau', 'TKL', 'Tokelau', 'https://restcountries.eu/data/tkl.svg', NULL, NULL),
(229, 5, 'Tonga', 'TON', 'Tonga', 'https://restcountries.eu/data/ton.svg', NULL, NULL),
(230, 2, 'Trinidad and Tobago', 'TTO', 'Trinidad and Tobago', 'https://restcountries.eu/data/tto.svg', NULL, NULL),
(231, 1, 'Tunisia', 'TUN', 'Tunisia', 'https://restcountries.eu/data/tun.svg', NULL, NULL),
(232, 3, 'Turkey', 'TUR', 'Turkey', 'https://restcountries.eu/data/tur.svg', NULL, NULL),
(233, 3, 'Turkmenistan', 'TKM', 'Turkmenistan', 'https://restcountries.eu/data/tkm.svg', NULL, NULL),
(234, 2, 'Turks and Caicos Islands', 'TCA', 'Turks and Caicos Islands', 'https://restcountries.eu/data/tca.svg', NULL, NULL),
(235, 5, 'Tuvalu', 'TUV', 'Tuvalu', 'https://restcountries.eu/data/tuv.svg', NULL, NULL),
(236, 1, 'Uganda', 'UGA', 'Uganda', 'https://restcountries.eu/data/uga.svg', NULL, NULL),
(237, 4, 'Ukraine', 'UKR', 'Ukraine', 'https://restcountries.eu/data/ukr.svg', NULL, NULL),
(238, 3, 'United Arab Emirates', 'ARE', 'United Arab Emirates', 'https://restcountries.eu/data/are.svg', NULL, NULL),
(239, 4, 'United Kingdom of Great Britain and Northern Ireland', 'GBR', 'United Kingdom of Great Britain and Northern Ireland', 'https://restcountries.eu/data/gbr.svg', NULL, NULL),
(240, 2, 'United States of America', 'USA', 'United States of America', 'https://restcountries.eu/data/usa.svg', NULL, NULL),
(241, 2, 'Uruguay', 'URY', 'Uruguay', 'https://restcountries.eu/data/ury.svg', NULL, NULL),
(242, 3, 'Uzbekistan', 'UZB', 'Uzbekistan', 'https://restcountries.eu/data/uzb.svg', NULL, NULL),
(243, 5, 'Vanuatu', 'VUT', 'Vanuatu', 'https://restcountries.eu/data/vut.svg', NULL, NULL),
(244, 2, 'Venezuela (Bolivarian Republic of)', 'VEN', 'Venezuela (Bolivarian Republic of)', 'https://restcountries.eu/data/ven.svg', NULL, NULL),
(245, 3, 'Viet Nam', 'VNM', 'Viet Nam', 'https://restcountries.eu/data/vnm.svg', NULL, NULL),
(246, 5, 'Wallis and Futuna', 'WLF', 'Wallis and Futuna', 'https://restcountries.eu/data/wlf.svg', NULL, NULL),
(247, 1, 'Western Sahara', 'ESH', 'Western Sahara', 'https://restcountries.eu/data/esh.svg', NULL, NULL),
(248, 3, 'Yemen', 'YEM', 'Yemen', 'https://restcountries.eu/data/yem.svg', NULL, NULL),
(249, 1, 'Zambia', 'ZMB', 'Zambia', 'https://restcountries.eu/data/zmb.svg', NULL, NULL),
(250, 1, 'Zimbabwe', 'ZWE', 'Zimbabwe', 'https://restcountries.eu/data/zwe.svg', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `courses`
--

DROP TABLE IF EXISTS `courses`;
CREATE TABLE `courses` (
  `id` int(10) UNSIGNED NOT NULL,
  `education_level_id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `fee` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `starting_date` date DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `licence_required` tinyint(1) NOT NULL DEFAULT '0',
  `licence_id` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `course_service_mode`
--

DROP TABLE IF EXISTS `course_service_mode`;
CREATE TABLE `course_service_mode` (
  `id` int(10) UNSIGNED NOT NULL,
  `course_id` int(10) UNSIGNED NOT NULL,
  `mode` enum('home','tuition','remote') COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `departments`
--

DROP TABLE IF EXISTS `departments`;
CREATE TABLE `departments` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `created_by` int(10) UNSIGNED NOT NULL,
  `approved_by` int(10) UNSIGNED DEFAULT NULL,
  `approved_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `departments`
--

INSERT INTO `departments` (`id`, `title`, `status`, `created_by`, `approved_by`, `approved_at`, `created_at`, `updated_at`) VALUES
(2, 'CSE', 0, 13, NULL, NULL, '2018-12-30 16:07:13', '2018-12-30 16:07:13'),
(3, 'EEE', 0, 13, NULL, NULL, '2018-12-30 16:07:18', '2018-12-30 16:07:18'),
(4, 'Administration', 0, 15, NULL, NULL, '2019-01-02 10:24:54', '2019-01-02 10:24:54'),
(5, 'Computer Science', 0, 24, NULL, NULL, '2019-01-06 16:37:26', '2019-01-06 16:37:26'),
(6, 'School Teacher', 0, 15, NULL, NULL, '2019-01-29 16:41:55', '2019-01-29 16:41:55'),
(7, 'Admission Office', 0, 15, NULL, NULL, '2019-01-29 16:42:17', '2019-01-29 16:42:17');

-- --------------------------------------------------------

--
-- Table structure for table `education`
--

DROP TABLE IF EXISTS `education`;
CREATE TABLE `education` (
  `id` int(10) UNSIGNED NOT NULL,
  `grade_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` int(11) NOT NULL,
  `grade` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `institute` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `completion_date` date DEFAULT NULL,
  `percentage_obtained` int(11) DEFAULT NULL,
  `education_level` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `educations`
--

DROP TABLE IF EXISTS `educations`;
CREATE TABLE `educations` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `institute_id` int(10) UNSIGNED DEFAULT NULL,
  `education_level_id` int(10) UNSIGNED DEFAULT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `grade` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `completion_date` date DEFAULT NULL,
  `percentage` int(11) DEFAULT NULL,
  `gpa` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `educations`
--

INSERT INTO `educations` (`id`, `user_id`, `institute_id`, `education_level_id`, `title`, `grade`, `completion_date`, `percentage`, `gpa`, `created_at`, `updated_at`) VALUES
(1, 1, 27, 17, '6th Grade', 'A', '2019-02-05', 85, NULL, '2019-05-14 12:24:47', '2019-05-14 12:24:47');

-- --------------------------------------------------------

--
-- Table structure for table `education_levels`
--

DROP TABLE IF EXISTS `education_levels`;
CREATE TABLE `education_levels` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `created_by` int(10) UNSIGNED NOT NULL,
  `approved_by` int(10) UNSIGNED DEFAULT NULL,
  `approved_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `education_levels`
--

INSERT INTO `education_levels` (`id`, `title`, `status`, `created_by`, `approved_by`, `approved_at`, `created_at`, `updated_at`) VALUES
(1, 'i.o.s', 0, 2, 1, '2018-11-09 05:20:30', '2018-11-29 23:52:46', '2018-12-08 12:41:49'),
(2, 'q.q.b', 1, 1, 3, '2018-11-09 23:29:52', '2018-11-08 23:40:48', '2018-12-08 12:41:49'),
(3, 'm.l.p', 0, 4, 3, '2018-11-13 08:01:34', '2018-11-24 23:05:24', '2018-12-08 12:41:49'),
(4, 'h.k.z', 0, 2, 2, '2018-11-13 01:50:20', '2018-11-13 17:56:15', '2018-12-08 12:41:49'),
(5, 'n.a.s', 1, 1, 4, '2018-11-28 22:10:07', '2018-11-15 13:34:00', '2018-12-08 12:41:49'),
(6, 'v.v.k', 1, 3, 2, '2018-11-21 21:44:35', '2018-11-28 22:30:45', '2018-12-08 12:41:49'),
(7, 'p.f.i', 0, 4, 1, '2018-11-27 02:50:57', '2018-11-08 16:52:29', '2018-12-08 12:41:49'),
(8, 'g.x.y', 0, 1, 1, '2018-11-12 09:18:50', '2018-11-28 03:45:41', '2018-12-08 12:41:49'),
(9, 'k.r.x', 1, 2, 4, '2018-11-28 08:52:42', '2018-11-18 11:29:38', '2018-12-08 12:41:49'),
(10, 'd.f.i', 1, 2, 2, '2018-11-15 10:20:35', '2018-11-27 18:39:50', '2018-12-08 12:41:49'),
(11, 'Matric', 0, 11, NULL, NULL, '2018-12-13 04:10:26', '2018-12-13 04:10:26'),
(12, 'Graduation', 0, 11, NULL, NULL, '2018-12-28 14:58:02', '2018-12-28 14:58:02'),
(13, 'A-Levels', 0, 15, NULL, NULL, '2019-01-02 10:39:05', '2019-01-02 10:39:05'),
(14, 'Certification', 0, 18, NULL, NULL, '2019-01-04 05:21:08', '2019-01-04 05:21:08'),
(15, 'Master', 0, 22, NULL, NULL, '2019-01-06 16:25:17', '2019-01-06 16:25:17'),
(16, 'MS', 0, 15, NULL, NULL, '2019-01-27 17:39:59', '2019-01-27 17:39:59'),
(17, 'Elementry', 0, 1, NULL, NULL, '2019-05-14 12:24:21', '2019-05-14 12:24:21'),
(18, 'High School', 0, 2, NULL, NULL, '2019-05-14 14:29:37', '2019-05-14 14:29:37');

-- --------------------------------------------------------

--
-- Table structure for table `experiences`
--

DROP TABLE IF EXISTS `experiences`;
CREATE TABLE `experiences` (
  `id` int(10) UNSIGNED NOT NULL,
  `institute_id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `from_date` date NOT NULL,
  `to_date` date DEFAULT NULL,
  `continue` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

DROP TABLE IF EXISTS `failed_jobs`;
CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `friendships`
--

DROP TABLE IF EXISTS `friendships`;
CREATE TABLE `friendships` (
  `id` int(10) UNSIGNED NOT NULL,
  `sender_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `sender_id` bigint(20) UNSIGNED NOT NULL,
  `recipient_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `recipient_id` bigint(20) UNSIGNED NOT NULL,
  `message` text COLLATE utf8mb4_unicode_ci,
  `status` tinyint(4) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `friendship_social_invites`
--

DROP TABLE IF EXISTS `friendship_social_invites`;
CREATE TABLE `friendship_social_invites` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `custom_message` text COLLATE utf8mb4_unicode_ci,
  `short_url` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `ielts`
--

DROP TABLE IF EXISTS `ielts`;
CREATE TABLE `ielts` (
  `id` int(10) UNSIGNED NOT NULL,
  `ielts_exam_level_id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `score` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `taken_on` date DEFAULT NULL,
  `grade` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `ielts_exam_levels`
--

DROP TABLE IF EXISTS `ielts_exam_levels`;
CREATE TABLE `ielts_exam_levels` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `created_by` int(10) UNSIGNED NOT NULL,
  `approved_by` int(10) UNSIGNED DEFAULT NULL,
  `approved_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `ielts_exam_levels`
--

INSERT INTO `ielts_exam_levels` (`id`, `title`, `status`, `created_by`, `approved_by`, `approved_at`, `created_at`, `updated_at`) VALUES
(1, 'IELTS', 0, 11, NULL, NULL, '2018-12-09 17:46:11', '2018-12-09 17:46:11'),
(2, 'Tofel', 0, 11, NULL, NULL, '2018-12-13 04:13:32', '2018-12-13 04:13:32');

-- --------------------------------------------------------

--
-- Table structure for table `institutes`
--

DROP TABLE IF EXISTS `institutes`;
CREATE TABLE `institutes` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `education_level_id` int(10) UNSIGNED DEFAULT NULL,
  `phone` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `institutes`
--

INSERT INTO `institutes` (`id`, `title`, `education_level_id`, `phone`, `email`, `user_id`, `created_by`, `created_at`, `updated_at`) VALUES
(1, 'magni Institute', 10, '227-391-4136', 'monroe.rempel@example.org', NULL, NULL, '2018-11-27 08:53:26', '2018-12-02 18:25:47'),
(2, 'et Institute', 8, '(623) 621-3529', 'mprice@example.com', NULL, NULL, '2018-11-18 19:10:19', '2018-11-20 20:05:49'),
(3, 'ipsum Institute', 5, '+1-352-473-8119', 'beahan.amos@example.net', NULL, NULL, '2018-11-15 21:17:37', '2018-11-10 01:19:52'),
(4, 'et Institute', 2, '996.389.7820 x56201', 'thartmann@example.com', NULL, NULL, '2018-11-10 10:58:27', '2018-11-17 19:13:21'),
(5, 'architecto Institute', 3, '+1-842-835-9771', 'micah03@example.com', NULL, NULL, '2018-11-27 17:34:11', '2018-11-25 15:25:22'),
(6, 'illum Institute', 2, '(885) 606-9373', 'sfriesen@example.com', NULL, NULL, '2018-11-25 19:58:44', '2018-12-02 16:07:48'),
(7, 'fugit Institute', 3, '450-662-7709 x07434', 'jnolan@example.org', NULL, NULL, '2018-12-08 03:19:25', '2018-11-10 11:14:28'),
(8, 'illo Institute', 5, '1-960-500-3507 x469', 'burnice.wiza@example.net', NULL, NULL, '2018-11-16 01:24:21', '2018-11-26 12:30:17'),
(9, 'dolorem Institute', 10, '+1-645-677-5706', 'ukessler@example.net', NULL, NULL, '2018-12-02 01:37:37', '2018-12-02 14:18:18'),
(10, 'quod Institute', 10, '360-643-1532', 'jo.bradtke@example.com', NULL, NULL, '2018-11-27 00:31:43', '2018-11-24 06:09:06'),
(11, 'Institute Append', NULL, NULL, NULL, NULL, NULL, '2018-12-08 13:08:16', '2018-12-08 13:08:16'),
(12, 'Rawalpindi Board', NULL, NULL, NULL, NULL, NULL, '2018-12-13 04:10:06', '2018-12-13 04:10:06'),
(27, 'Portola Springs Elementary', NULL, NULL, NULL, 1, 1, '2019-05-14 12:24:43', '2019-05-14 12:24:43'),
(28, 'Irvine High School', 18, '(949) 936-7000', 'info@irvinehigh.iusd.org', 2, 2, '2019-05-14 14:30:35', '2019-05-14 14:30:35');

-- --------------------------------------------------------

--
-- Table structure for table `institute_author_user`
--

DROP TABLE IF EXISTS `institute_author_user`;
CREATE TABLE `institute_author_user` (
  `id` int(10) UNSIGNED NOT NULL,
  `institute_id` int(10) UNSIGNED DEFAULT NULL,
  `department_id` int(10) UNSIGNED DEFAULT NULL,
  `institute_role_id` int(10) UNSIGNED DEFAULT NULL,
  `created_by` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `mobile` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `institute_author_user`
--

INSERT INTO `institute_author_user` (`id`, `institute_id`, `department_id`, `institute_role_id`, `created_by`, `name`, `email`, `phone`, `mobile`, `created_at`, `updated_at`) VALUES
(1, 28, 7, 1, 2, 'Monica Colunga', 'monicacolunga@iusd.org', '(949) 936-7005', '(949) 936-7005', '2019-05-14 14:28:51', '2019-05-14 14:30:35'),
(2, NULL, 6, 3, 3, 'Jay Cassity', 'JayCassity@iusd.org', '+1 949-936-7000', '949-936-7000', '2019-05-14 14:43:36', '2019-05-14 14:43:36');

-- --------------------------------------------------------

--
-- Table structure for table `institute_roles`
--

DROP TABLE IF EXISTS `institute_roles`;
CREATE TABLE `institute_roles` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `institute_roles`
--

INSERT INTO `institute_roles` (`id`, `title`, `slug`, `created_at`, `updated_at`) VALUES
(1, 'Administration', 'administration', '2018-12-30 00:00:00', '2018-12-30 00:00:00'),
(2, 'Marketing', 'marketing', '2018-12-30 00:00:00', '2018-12-30 00:00:00'),
(3, 'Lecturer', 'Lecturer', '2018-12-30 00:00:00', '2018-12-30 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `interests`
--

DROP TABLE IF EXISTS `interests`;
CREATE TABLE `interests` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `since` date DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `interests`
--

INSERT INTO `interests` (`id`, `user_id`, `title`, `since`, `created_at`, `updated_at`) VALUES
(1, 1, 'NBA', '2019-06-04', '2019-05-14 12:25:12', '2019-05-14 12:25:12');

-- --------------------------------------------------------

--
-- Table structure for table `invitations`
--

DROP TABLE IF EXISTS `invitations`;
CREATE TABLE `invitations` (
  `id` int(10) UNSIGNED NOT NULL,
  `invited_from` int(10) UNSIGNED NOT NULL,
  `invite_to` int(10) UNSIGNED DEFAULT NULL,
  `invitable_id` int(10) UNSIGNED NOT NULL,
  `invitable_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `message` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '2',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `jobs`
--

DROP TABLE IF EXISTS `jobs`;
CREATE TABLE `jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `queue` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `attempts` tinyint(3) UNSIGNED NOT NULL,
  `reserved_at` int(10) UNSIGNED DEFAULT NULL,
  `available_at` int(10) UNSIGNED NOT NULL,
  `created_at` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `licences`
--

DROP TABLE IF EXISTS `licences`;
CREATE TABLE `licences` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `authority` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `start_date` date NOT NULL,
  `valid_till` date NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `medias`
--

DROP TABLE IF EXISTS `medias`;
CREATE TABLE `medias` (
  `id` int(10) UNSIGNED NOT NULL,
  `mediable_id` int(10) UNSIGNED NOT NULL,
  `mediable_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `filename` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `mime_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ext` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `size` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `original_filename` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `medias`
--

INSERT INTO `medias` (`id`, `mediable_id`, `mediable_type`, `title`, `filename`, `mime_type`, `ext`, `size`, `original_filename`, `status`, `created_at`, `updated_at`) VALUES
(2, 1, 'App\\User', NULL, '14_12_12_36_917800.png', 'image/png', 'png', '54277', 'images.png', NULL, '2019-05-14 12:12:36', '2019-05-14 12:12:36'),
(3, 2, 'App\\User', NULL, '14_14_30_37_288400.png', 'image/png', 'png', '323256', 'Screen Shot 2019-05-14 at 7.26.31 PM.png', NULL, '2019-05-14 14:30:37', '2019-05-14 14:30:37');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2018_11_15_132208_create_roles_table', 1),
(4, '2018_11_17_121815_create_profile_options_table', 1),
(5, '2018_11_17_121854_create_education_table', 1),
(6, '2018_11_17_122037_create_videos_table', 1),
(7, '2018_11_20_081154_create_interests_table', 1),
(8, '2018_11_23_123042_create_social_providers_table', 1),
(9, '2018_11_23_124131_create_profiles_table', 1),
(10, '2018_11_23_124204_create_regions_table', 1),
(11, '2018_11_23_124222_create_countries_table', 1),
(12, '2018_11_23_124233_create_states_table', 1),
(13, '2018_11_23_124316_create_addresses_table', 1),
(14, '2018_11_23_125405_create_education_levels_table', 1),
(15, '2018_11_23_125614_create_institutes_table', 1),
(16, '2018_11_23_125648_create_educations_table', 1),
(17, '2018_11_23_125726_create_ielts_exam_levels_table', 1),
(18, '2018_11_23_125741_create_ielts_table', 1),
(19, '2018_11_23_125827_create_medias_table', 1),
(20, '2018_11_23_125845_create_invitations_table', 1),
(21, '2018_11_23_125914_create_departments_table', 1),
(22, '2018_11_23_130018_create_institute_roles_table', 1),
(23, '2018_11_23_130103_create_institute_author_user_table', 1),
(24, '2018_11_23_130218_create_experiences_table', 1),
(25, '2018_11_23_130237_create_licences_table', 1),
(26, '2018_11_23_130256_create_programs_table', 1),
(27, '2018_11_23_130312_create_courses_table', 1),
(28, '2018_11_23_130335_create_course_service_mode_table', 1),
(29, '2018_12_19_134150_create_notifications_table', 2),
(30, '2018_12_28_080810_create_chattings_table', 2),
(31, '2019_01_08_175732_create_friendships_table', 3),
(32, '2019_01_08_175733_create_friendships_groups_table', 3),
(33, '2019_01_15_150735_create_table_posts', 4),
(34, '2019_01_15_151615_create_table_comments', 4),
(35, '2019_02_14_101131_create_tweets_table', 5),
(36, '2019_03_07_142635_create_jobs_table', 5),
(37, '2019_03_07_143156_create_failed_jobs_table', 5);

-- --------------------------------------------------------

--
-- Table structure for table `notifications`
--

DROP TABLE IF EXISTS `notifications`;
CREATE TABLE `notifications` (
  `id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `notifiable_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `notifiable_id` bigint(20) UNSIGNED NOT NULL,
  `data` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `read_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

DROP TABLE IF EXISTS `password_resets`;
CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `posts`
--

DROP TABLE IF EXISTS `posts`;
CREATE TABLE `posts` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1',
  `body` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `profiles`
--

DROP TABLE IF EXISTS `profiles`;
CREATE TABLE `profiles` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `gender` enum('male','female','others') COLLATE utf8mb4_unicode_ci NOT NULL,
  `date_of_birth` date NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `profile_options`
--

DROP TABLE IF EXISTS `profile_options`;
CREATE TABLE `profile_options` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `option_key` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `option_value` text COLLATE utf8mb4_unicode_ci,
  `type` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `profile_options`
--

INSERT INTO `profile_options` (`id`, `user_id`, `option_key`, `option_value`, `type`, `created_at`, `updated_at`) VALUES
(1, 1, 'date_of_birth', '2005-01-01', NULL, '2019-05-14 12:13:12', '2019-05-14 12:13:12'),
(2, 1, 'gender', 'Male', NULL, '2019-05-14 12:13:12', '2019-05-14 12:13:12'),
(3, 1, 'facebook_url', NULL, NULL, '2019-05-14 12:22:36', '2019-05-14 12:22:36'),
(4, 1, 'twitter_url', 'https://twitter.com/FaizanA46337390', NULL, '2019-05-14 12:22:36', '2019-05-14 12:22:36'),
(5, 1, 'instragram_url', NULL, NULL, '2019-05-14 12:22:36', '2019-05-14 12:22:36'),
(6, 1, 'google_url', NULL, NULL, '2019-05-14 12:22:36', '2019-05-14 12:22:36'),
(7, 2, 'facebook_url', NULL, NULL, '2019-05-14 14:32:27', '2019-05-14 14:32:27'),
(8, 2, 'twitter_url', 'https://twitter.com/IHSVaqueros', NULL, '2019-05-14 14:32:27', '2019-05-14 14:32:27'),
(9, 2, 'instragram_url', 'https://www.instagram.com/ihsvaqueros/', NULL, '2019-05-14 14:32:27', '2019-05-14 14:32:27'),
(10, 2, 'google_url', NULL, NULL, '2019-05-14 14:32:27', '2019-05-14 14:32:27');

-- --------------------------------------------------------

--
-- Table structure for table `programs`
--

DROP TABLE IF EXISTS `programs`;
CREATE TABLE `programs` (
  `id` int(10) UNSIGNED NOT NULL,
  `address_id` int(10) UNSIGNED NOT NULL,
  `education_level_id` int(10) UNSIGNED NOT NULL,
  `institute_id` int(10) UNSIGNED NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `fee` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `admission_date` date DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `programs`
--

INSERT INTO `programs` (`id`, `address_id`, `education_level_id`, `institute_id`, `title`, `fee`, `admission_date`, `description`, `created_at`, `updated_at`) VALUES
(1, 2, 18, 28, 'H American Literature', '$700', '2019-06-01', 'Irvine High School has a 4/4 Semester Block Schedule. The academic year is comprised of two semesters. The school day is organized into four (4) ninety minute blocks. Students have the opportunity to take some classes in solid blocks and to take other classes in alternating blocks.', '2019-05-14 14:35:52', '2019-05-14 14:37:23'),
(2, 2, 18, 28, 'Science', '$1000', '2019-06-08', 'The importance of science in modern living and the need for scientific literacy on the part of all citizens are widely accepted beliefs. The curriculum, therefore, is designed around multiple course offerings for students over a wide range of interests and abilities. Moreover, the curriculum reflects the educational demands for living in our scientific technological-industrialized society as well as the human values of this age.', '2019-05-14 14:39:34', '2019-05-14 14:39:34');

-- --------------------------------------------------------

--
-- Table structure for table `regions`
--

DROP TABLE IF EXISTS `regions`;
CREATE TABLE `regions` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `regions`
--

INSERT INTO `regions` (`id`, `title`, `slug`, `created_at`, `updated_at`) VALUES
(1, 'Africa', 'africa', NULL, NULL),
(2, 'Americas', 'americas', NULL, NULL),
(3, 'Asia', 'asia', NULL, NULL),
(4, 'Europe', 'europe', NULL, NULL),
(5, 'Oceania', 'oceania', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

DROP TABLE IF EXISTS `roles`;
CREATE TABLE `roles` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `title`, `slug`, `created_at`, `updated_at`) VALUES
(1, 'Admin', 'admin', '2018-12-08 12:41:49', '2018-12-08 12:41:49'),
(2, 'Student', 'student', '2018-12-08 12:41:49', '2018-12-08 12:41:49'),
(3, 'School', 'school', '2018-12-08 12:41:49', '2018-12-08 12:41:49'),
(4, 'Tutor', 'tutor', '2018-12-08 12:41:49', '2018-12-08 12:41:49'),
(5, 'Counselor', 'counselor', '2018-12-08 12:41:49', '2018-12-08 12:41:49'),
(6, 'Parent', 'parent', '2018-12-08 12:41:49', '2018-12-08 12:41:49');

-- --------------------------------------------------------

--
-- Table structure for table `settings`
--

DROP TABLE IF EXISTS `settings`;
CREATE TABLE `settings` (
  `id` int(10) UNSIGNED NOT NULL DEFAULT '1',
  `upgrade_amount` decimal(10,2) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `settings`
--

INSERT INTO `settings` (`id`, `upgrade_amount`, `created_at`, `updated_at`) VALUES
(1, '1.00', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `social_message_pendings`
--

DROP TABLE IF EXISTS `social_message_pendings`;
CREATE TABLE `social_message_pendings` (
  `id` int(10) UNSIGNED NOT NULL,
  `sender_id` int(11) NOT NULL,
  `receiver_id` int(11) NOT NULL,
  `platform` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `hash` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `social_providers`
--

DROP TABLE IF EXISTS `social_providers`;
CREATE TABLE `social_providers` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `provider_id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `refresh_token` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `token_secret` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `token_expire` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `provider` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `url` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `social_topics`
--

DROP TABLE IF EXISTS `social_topics`;
CREATE TABLE `social_topics` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED DEFAULT NULL,
  `search_query` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `queue_status` enum('Listening','Pause','Stop') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'Stop',
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `states`
--

DROP TABLE IF EXISTS `states`;
CREATE TABLE `states` (
  `id` int(10) UNSIGNED NOT NULL,
  `country_id` int(10) UNSIGNED NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `states`
--

INSERT INTO `states` (`id`, `country_id`, `title`, `created_at`, `updated_at`) VALUES
(1, 170, 'Azad Jammu & Kashmir', NULL, NULL),
(2, 170, 'Balochistan', NULL, NULL),
(3, 170, 'Gilgit-Baltistan', NULL, NULL),
(4, 170, 'Islamabad Capital Territory', NULL, NULL),
(5, 170, 'Khyber Pakhtunkhwa', NULL, NULL),
(6, 170, '	Punjab', NULL, NULL),
(7, 170, '	Sindh', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tweets`
--

DROP TABLE IF EXISTS `tweets`;
CREATE TABLE `tweets` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED DEFAULT NULL,
  `tweet_id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tweet_user_id` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tweet_screen_name` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tweet_user_name` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tweet_user_image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tweet_created_at` datetime DEFAULT NULL,
  `tweet_text` text COLLATE utf8mb4_unicode_ci,
  `tweet` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `category` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `entities` text COLLATE utf8mb4_unicode_ci,
  `user_sentiment` enum('Positive','Negative','Neutral') COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `predictied_sentiment` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `classifications` text COLLATE utf8mb4_unicode_ci,
  `concepts` text COLLATE utf8mb4_unicode_ci,
  `search_query` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_enriched` enum('Yes','No') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'No',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `upgrades`
--

DROP TABLE IF EXISTS `upgrades`;
CREATE TABLE `upgrades` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `payment_type` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `payment_date` date NOT NULL,
  `card_number` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `card_full_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `card_expire` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `card_cvc` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `amount` decimal(10,2) UNSIGNED NOT NULL,
  `payment_info` longtext COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `role_id` int(11) NOT NULL DEFAULT '2',
  `premium_user` tinyint(3) UNSIGNED NOT NULL DEFAULT '0' COMMENT '0 => No, 1 => Yes',
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `role_id`, `premium_user`, `name`, `user_name`, `email`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 2, 0, 'Faizan Ali', 'faizan-ali', 'faizanali2012a@gmail.com', '2019-05-14 12:19:13', '$2y$10$Yf7yt9NkzeVhYSAis/ba8OE9NnnPVMZSnNkW3NrVu/.3HZ51X7CmO', '$2y$10$Yf7yt9NkzeVhYSAis/ba8OE9NnnPVMZSnNkW3NrVu/.3HZ51X7CmO', '2019-05-14 12:08:19', '2019-05-14 12:19:13'),
(2, 3, 0, 'Irvine High School', 'irvine-high-school', 'info@irvinehigh.iusd.org', NULL, '$2y$10$Qx1h7h7c4Te/.PKLLC7.UelojSsjlY727.sCuYeeGDRDPwcpj8VOm', '5GdhgKrDR1CMpab0uHpFY3HPD1BKVHCv6gtAcrLiLcy3MgWUIaZkA45Sej6h', '2019-05-14 14:27:10', '2019-05-14 14:27:10'),
(3, 3, 0, 'Jay Cassity', 'jay-cassity', 'JayCassity@iusd.org', NULL, '$2y$10$dcuj9It0RuFYxtoiuI/s4.SVuBEljZCuQQAZ85SvEaf6LD5YtWcQ6', 'K89vOi6hZ7DYD6klNVIS23BAqSRb7ADZa6Nkfg2mzN9nJZG5nknNiGT7yomR', '2019-05-14 14:42:30', '2019-05-14 14:42:30');

-- --------------------------------------------------------

--
-- Table structure for table `user_friendship_groups`
--

DROP TABLE IF EXISTS `user_friendship_groups`;
CREATE TABLE `user_friendship_groups` (
  `friendship_id` int(10) UNSIGNED NOT NULL,
  `friend_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `friend_id` bigint(20) UNSIGNED NOT NULL,
  `group_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `videos`
--

DROP TABLE IF EXISTS `videos`;
CREATE TABLE `videos` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `video` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `addresses`
--
ALTER TABLE `addresses`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `chattings`
--
ALTER TABLE `chattings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `chatting_blocks`
--
ALTER TABLE `chatting_blocks`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `user_id` (`from`,`to`);

--
-- Indexes for table `comments`
--
ALTER TABLE `comments`
  ADD PRIMARY KEY (`id`),
  ADD KEY `comments_post_id_foreign` (`post_id`);

--
-- Indexes for table `countries`
--
ALTER TABLE `countries`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `countries_title_unique` (`title`),
  ADD UNIQUE KEY `countries_code_unique` (`code`),
  ADD UNIQUE KEY `countries_slug_unique` (`slug`);

--
-- Indexes for table `courses`
--
ALTER TABLE `courses`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `course_service_mode`
--
ALTER TABLE `course_service_mode`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `departments`
--
ALTER TABLE `departments`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `departments_title_unique` (`title`);

--
-- Indexes for table `education`
--
ALTER TABLE `education`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `educations`
--
ALTER TABLE `educations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `education_levels`
--
ALTER TABLE `education_levels`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `education_levels_title_unique` (`title`);

--
-- Indexes for table `experiences`
--
ALTER TABLE `experiences`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `friendships`
--
ALTER TABLE `friendships`
  ADD PRIMARY KEY (`id`),
  ADD KEY `friendships_sender_type_sender_id_index` (`sender_type`,`sender_id`),
  ADD KEY `friendships_recipient_type_recipient_id_index` (`recipient_type`,`recipient_id`);

--
-- Indexes for table `friendship_social_invites`
--
ALTER TABLE `friendship_social_invites`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ielts`
--
ALTER TABLE `ielts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ielts_exam_levels`
--
ALTER TABLE `ielts_exam_levels`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `ielts_exam_levels_title_unique` (`title`);

--
-- Indexes for table `institutes`
--
ALTER TABLE `institutes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `institute_author_user`
--
ALTER TABLE `institute_author_user`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `institute_roles`
--
ALTER TABLE `institute_roles`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `institute_roles_slug_unique` (`slug`);

--
-- Indexes for table `interests`
--
ALTER TABLE `interests`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `invitations`
--
ALTER TABLE `invitations`
  ADD PRIMARY KEY (`id`),
  ADD KEY `invitations_status_index` (`status`);

--
-- Indexes for table `jobs`
--
ALTER TABLE `jobs`
  ADD PRIMARY KEY (`id`),
  ADD KEY `jobs_queue_index` (`queue`);

--
-- Indexes for table `licences`
--
ALTER TABLE `licences`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `medias`
--
ALTER TABLE `medias`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `notifications`
--
ALTER TABLE `notifications`
  ADD PRIMARY KEY (`id`),
  ADD KEY `notifications_notifiable_type_notifiable_id_index` (`notifiable_type`,`notifiable_id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `posts`
--
ALTER TABLE `posts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `profiles`
--
ALTER TABLE `profiles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `profile_options`
--
ALTER TABLE `profile_options`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `programs`
--
ALTER TABLE `programs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `regions`
--
ALTER TABLE `regions`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `regions_title_unique` (`title`),
  ADD UNIQUE KEY `regions_slug_unique` (`slug`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `roles_slug_unique` (`slug`);

--
-- Indexes for table `social_message_pendings`
--
ALTER TABLE `social_message_pendings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `social_providers`
--
ALTER TABLE `social_providers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `social_topics`
--
ALTER TABLE `social_topics`
  ADD PRIMARY KEY (`id`),
  ADD KEY `tweets_user_id_foreign` (`user_id`);

--
-- Indexes for table `states`
--
ALTER TABLE `states`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tweets`
--
ALTER TABLE `tweets`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `tweets_tweet_id_unique` (`tweet_id`),
  ADD KEY `tweets_user_id_foreign` (`user_id`);

--
-- Indexes for table `upgrades`
--
ALTER TABLE `upgrades`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_user_name_unique` (`user_name`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- Indexes for table `user_friendship_groups`
--
ALTER TABLE `user_friendship_groups`
  ADD UNIQUE KEY `unique` (`friendship_id`,`friend_id`,`friend_type`,`group_id`),
  ADD KEY `user_friendship_groups_friend_type_friend_id_index` (`friend_type`,`friend_id`);

--
-- Indexes for table `videos`
--
ALTER TABLE `videos`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `addresses`
--
ALTER TABLE `addresses`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `chattings`
--
ALTER TABLE `chattings`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `chatting_blocks`
--
ALTER TABLE `chatting_blocks`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `comments`
--
ALTER TABLE `comments`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `countries`
--
ALTER TABLE `countries`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=251;
--
-- AUTO_INCREMENT for table `courses`
--
ALTER TABLE `courses`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `course_service_mode`
--
ALTER TABLE `course_service_mode`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `departments`
--
ALTER TABLE `departments`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `education`
--
ALTER TABLE `education`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `educations`
--
ALTER TABLE `educations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `education_levels`
--
ALTER TABLE `education_levels`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;
--
-- AUTO_INCREMENT for table `experiences`
--
ALTER TABLE `experiences`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `friendships`
--
ALTER TABLE `friendships`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `friendship_social_invites`
--
ALTER TABLE `friendship_social_invites`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `ielts`
--
ALTER TABLE `ielts`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `ielts_exam_levels`
--
ALTER TABLE `ielts_exam_levels`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `institutes`
--
ALTER TABLE `institutes`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;
--
-- AUTO_INCREMENT for table `institute_author_user`
--
ALTER TABLE `institute_author_user`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `institute_roles`
--
ALTER TABLE `institute_roles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `interests`
--
ALTER TABLE `interests`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `invitations`
--
ALTER TABLE `invitations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `jobs`
--
ALTER TABLE `jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `licences`
--
ALTER TABLE `licences`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `medias`
--
ALTER TABLE `medias`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=38;
--
-- AUTO_INCREMENT for table `posts`
--
ALTER TABLE `posts`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `profiles`
--
ALTER TABLE `profiles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `profile_options`
--
ALTER TABLE `profile_options`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `programs`
--
ALTER TABLE `programs`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `regions`
--
ALTER TABLE `regions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `social_message_pendings`
--
ALTER TABLE `social_message_pendings`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `social_providers`
--
ALTER TABLE `social_providers`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `social_topics`
--
ALTER TABLE `social_topics`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `states`
--
ALTER TABLE `states`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `tweets`
--
ALTER TABLE `tweets`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `upgrades`
--
ALTER TABLE `upgrades`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `videos`
--
ALTER TABLE `videos`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `comments`
--
ALTER TABLE `comments`
  ADD CONSTRAINT `comments_post_id_foreign` FOREIGN KEY (`post_id`) REFERENCES `posts` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `tweets`
--
ALTER TABLE `tweets`
  ADD CONSTRAINT `tweets_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

--
-- Constraints for table `user_friendship_groups`
--
ALTER TABLE `user_friendship_groups`
  ADD CONSTRAINT `user_friendship_groups_friendship_id_foreign` FOREIGN KEY (`friendship_id`) REFERENCES `friendships` (`id`) ON DELETE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
